from csr import *
import sys


def comm_hub(argv):
	beginFile('Comm_Hub', 0xC1, (0,5,0), argv = argv,
			brief	= 'M5 communications hub',
			detail	= StandardCsrDescription)
	beginCsr()
	#
	beginBits('any_fault_flag',			'U32',	'R',	'fault flags')
	bitField('faultConfig',				1,		'R',	'Configuration fault')
	bitField('faultNetwork',			1,		'R',	'Network fault')
	bitField('faultSerialServer',		1,		'R',	'Serial server fault')
	reserveBits(13)
	bitField('faultChannel1',			1,		'R',	'Channel 1 fault')
	bitField('faultChannel2',			1,		'R',	'Channel 2 fault')
	bitField('faultChannel3',			1,		'R',	'Channel 3 fault')
	bitField('faultChannel4',			1,		'R',	'Channel 4 fault')
	bitField('faultChannel5',			1,		'R',	'Channel 5 fault')
	bitField('faultChannel6',			1,		'R',	'Channel 6 fault')
	endBits()
	dataField('ch1_bw',					'F32',	'R',	'bandwidth bytes/Sec')
	dataField('ch2_bw',					'F32',	'R',	'bandwidth bytes/Sec')
	reserveBytes(16)
	#
	dataField('ch1_fault',				'U32',	'R',	'flags')
	dataField('ch1_net_tx_cnt',			'U32',	'R',	'byte count')
	dataField('ch1_net_rx_cnt',			'U32',	'R',	'byte count')
	dataField('ch1_uart_tx_cnt',		'U32',	'R',	'byte count')
	dataField('ch1_uart_rx_cnt',		'U32',	'R',	'byte count')
	dataField('ch2_fault',				'U32',	'R',	'flags')
	dataField('ch2_net_tx_cnt',			'U32',	'R',	'byte count')
	dataField('ch2_net_rx_cnt',			'U32',	'R',	'byte count')
	dataField('ch2_uart_tx_cnt',		'U32',	'R',	'byte count')
	dataField('ch2_uart_rx_cnt',		'U32',	'R',	'byte count')
	reserveBytes(88)
	#
	beginBits('system_flags',			'U8',	'RW',	'Configuration flags')
	bitField('use_dhcp',				1,		'RW',	'Use DHCP')
#e	reserveBits(8)
#	bitField('enable_auto_switch_12v',	1,		'RW',	'Enable 12V auto switch'
#	bitField('enable_auto_switch_24v',	1,		'RW',	'Enable 24V auto switch'
	endBits()
	reserveBytes(2)
	dataField('fault_interlock',		'U32',	'RW',	'password')
	dataField('fault_control',			'U32',	'RW',	'flag')
	reserveBytes(4)
	#
	dataField('ip_address',				'U32',	'RW',	'IP address')
	dataField('ip_netmask',				'U32',	'RW',	'IP net mast')
	dataField('ip_gateway',				'U32',	'RW',	'IP gateway')
	dataField('command_port',			'U16',	'?',	'Command port')
	reserveBytes(2)
	#
	dataField('ch1_HW_id',				'U8',	'RW',	'ordinal')
	dataField('ch1_type',				'U8',	'RW',	'HW type enum')
	dataField('ch1_port',				'U16',	'RW',	'server port')
	dataField('ch1_speed',				'U32',	'RW',	'baud rate')
	#
	dataField('ch2_HW_id',				'U8',	'RW',	'ordinal')
	dataField('ch2_type',				'U8',	'RW',	'HW type enum')
	dataField('ch2_port',				'U16',	'RW',	'server port')
	dataField('ch2_speed',				'U32',	'RW',	'baud rate')
	reserveBytes(16)
	#
	dataField('imu_type',				'U8',	'?',	'???')
	dataField('depth_senor_type',		'U8',	'?',	'???')
	dataField('depth_tx_rate',			'U16',	'?',	'???')
	#
	setByteOffset(238)
	beginGroup('Standard VideoRay device registers')
	dataField('save_settings',			'U16',	'W',	'Code')
	endGroup()
	#
	standardCsrRegisters()
	endCsr()
	endFile()


if __name__ == "__main__":
	comm_hub(sys.argv[1:])
