from csr import *
import sys


def test_1(testNumber, argv):
	beginFile('Csr_Compiler_Test_' + str(testNumber), 254, (0,1,1), argv = argv, path = 'test_suite',
			brief	= 'Test #' + str(testNumber) + ' of CSR compiler',
			detail	= """
						This is a test of the CSR compiler and is not intended for actual use.
						
						""" + StandardCsrDescription)
	define(' CSR_TEST ', "({})".format(testNumber),
			brief	= ' Test Number ',
			detail	= """
						This value indicates the CSR test number.
						""")
	beginCsr()
	#
	beginGroup('Check data types')
	dataField(name = 'u8',		type = 'u8',		brief = '1-byte unsigned integer')
	dataField(name = 'u8_',		type = 'u8 ',		brief = '1-byte unsigned integer')
	dataField(name = 'u_8',		type = 'u 8',		brief = '1-byte unsigned integer')
	dataField(name = 'u_8_',	type = 'u 8 ',		brief = '1-byte unsigned integer')
	dataField(name = '_u8',		type = ' u8',		brief = '1-byte unsigned integer')
	dataField(name = '_u8_',	type = ' u8 ',		brief = '1-byte unsigned integer')
	dataField(name = '_u_8',	type = ' u 8',		brief = '1-byte unsigned integer')
	dataField(name = '_u_8_',	type = ' u 8 ',		brief = '1-byte unsigned integer')
	dataField(name = 'U8',		type = 'U8',		brief = '1-byte unsigned integer')
	dataField(name = 'U8_',		type = 'U8 ',		brief = '1-byte unsigned integer')
	dataField(name = 'U_8',		type = 'U 8',		brief = '1-byte unsigned integer')
	dataField(name = 'U_8_',	type = 'U 8 ',		brief = '1-byte unsigned integer')
	dataField(name = '_U8',		type = ' U8',		brief = '1-byte unsigned integer')
	dataField(name = '_U8_',	type = ' U8 ',		brief = '1-byte unsigned integer')
	dataField(name = '_U_8',	type = ' U 8',		brief = '1-byte unsigned integer')
	dataField(name = '_U_8_',	type = ' U 8 ',		brief = '1-byte unsigned integer')
	dataField(name = 'u16',		type = 'u16',		brief = '2-byte unsigned integer')
	dataField(name = 'u16_',	type = 'u16 ',		brief = '2-byte unsigned integer')
	dataField(name = 'u1_6',	type = 'u1 6',		brief = '2-byte unsigned integer')
	dataField(name = 'u1_6_',	type = 'u1 6 ',		brief = '2-byte unsigned integer')
	dataField(name = 'u_16',	type = 'u 16',		brief = '2-byte unsigned integer')
	dataField(name = 'u_16_',	type = 'u 16 ',		brief = '2-byte unsigned integer')
	dataField(name = 'u_1_6',	type = 'u 1 6',		brief = '2-byte unsigned integer')
	dataField(name = 'u_1_6_',	type = 'u 1 6 ',	brief = '2-byte unsigned integer')
	dataField(name = '_u16',	type = ' u16',		brief = '2-byte unsigned integer')
	dataField(name = '_u16_',	type = ' u16 ',		brief = '2-byte unsigned integer')
	dataField(name = '_u1_6',	type = ' u1 6',		brief = '2-byte unsigned integer')
	dataField(name = '_u1_6_',	type = ' u1 6 ',	brief = '2-byte unsigned integer')
	dataField(name = '_u_16',	type = ' u 16',		brief = '2-byte unsigned integer')
	dataField(name = '_u_16_',	type = ' u 16 ',	brief = '2-byte unsigned integer')
	dataField(name = '_u_1_6',	type = ' u 1 6',	brief = '2-byte unsigned integer')
	dataField(name = '_u_1_6_',	type = ' u 1 6 ',	brief = '2-byte unsigned integer')
	dataField(name = 'U16',		type = 'U16',		brief = '2-byte unsigned integer')
	dataField(name = 'u32',		type = 'u32',		brief = '4-byte unsigned integer')
	dataField(name = 'U32',		type = 'U32',		brief = '4-byte unsigned integer')
	dataField(name = 'u64',		type = 'u64',		brief = '8-byte unsigned integer')
	dataField(name = 'U64',		type = 'U64',		brief = '8-byte unsigned integer')
	dataField(name = 's8',		type = 's8',		brief = '1-byte signed integer')
	dataField(name = 'S8',		type = 'S8',		brief = '1-byte signed integer')
	dataField(name = 's16',		type = 's16',		brief = '2-byte signed integer')
	dataField(name = 'S16',		type = 'S16',		brief = '2-byte signed integer')
	dataField(name = 's32',		type = 's32',		brief = '4-byte signed integer')
	dataField(name = 'S32',		type = 'S32',		brief = '4-byte signed integer')
	dataField(name = 's64',		type = 's64',		brief = '8-byte signed integer')
	dataField(name = 'S64',		type = 'S64',		brief = '8-byte signed integer')
	dataField(name = 'f32',		type = 'f32',		brief = '4-byte floating point')
	dataField(name = 'F32',		type = 'F32',		brief = '4-byte floating point')
	dataField(name = 'f64',		type = 'f64',		brief = '8-byte floating point')
	dataField(name = 'F64',		type = 'F64',		brief = '8-byte floating point')
	endGroup()
	#
	standardCsrRegisters()
	endCsr()
	endFile()


def test_2(testNumber, argv):
	beginFile('Csr_Compiler_Test_' + str(testNumber), 254, (0,1,2), argv = argv, path = 'test_suite',
			brief	= 'Test #' + str(testNumber) + ' of CSR compiler',
			detail	= """
						This is a test of the CSR compiler and is not intended for actual use.
						
						""" + StandardCsrDescription)
	define(' CSR_TEST ', "({})".format(testNumber),
			brief	= ' Test Number ',
			detail	= """
						This value indicates the CSR test number.
						""")
	beginCsr()
	#
	beginGroup('Check access codes')
	dataField(name = 'u8',		type = 'u8',	access = 'r',		brief = 'Access: r     - Read only')
	dataField(name = 'u8_',		type = 'u8 ',	access = 'r ',		brief = 'Access: r_    - Read only')
	dataField(name = 'u_8',		type = 'u 8',	access = ' r',		brief = 'Access: _r    - Read only')
	dataField(name = 'u_8_',	type = 'u 8 ',	access = ' r ',		brief = 'Access: _r_   - Read only')
	dataField(name = '_u8',		type = ' u8',	access = 'w',		brief = 'Access: w     - Write only')
	dataField(name = '_u8_',	type = ' u8 ',	access = 'w ',		brief = 'Access: w_    - Write only')
	dataField(name = '_u_8',	type = ' u 8',	access = ' w',		brief = 'Access: _w    - Write only')
	dataField(name = '_u_8_',	type = ' u 8 ',	access = ' w ',		brief = 'Access: _w_   - Write only')
	dataField(name = 'U8',		type = 'U8',	access = 'rw',		brief = 'Access: rw    - Read/write')
	dataField(name = 'U8_',		type = 'U8 ',	access = 'rw ',		brief = 'Access: rw_   - Read/write')
	dataField(name = 'U_8',		type = 'U 8',	access = ' rw',		brief = 'Access: _rw   - Read/write')
	dataField(name = 'U_8_',	type = 'U 8 ',	access = ' rw ',	brief = 'Access: _rw_  - Read/write')
	dataField(name = '_U8',		type = ' U8',	access = ' r w ',	brief = 'Access: _r_w_ - Read/write')
	dataField(name = '_U8_',	type = ' U8 ',	access = 'wr',		brief = 'Access: wR    - Read/write')
	dataField(name = '_U_8',	type = ' U 8',	access = 'R',		brief = 'Access: R     - Read only')
	dataField(name = '_U_8_',	type = ' U 8 ',	access = 'W',		brief = 'Access: W     - Write only')
	dataField(name = 'u16',		type = 'u16',	access = 'Rw',		brief = 'Access: Rw    - Read/write')
	dataField(name = 'u16_',	type = 'u16 ',	access = 'Wr',		brief = 'Access: Wr    - Read/write')
	dataField(name = 'u1_6',	type = 'u1 6',	access = 'rW',		brief = 'Access: rW    - Read/write')
	dataField(name = 'u1_6_',	type = 'u1 6 ',	access = 'RW',		brief = 'Access: RW    - Read/write')
	dataField(name = 'u_16',	type = 'u 16',	access = 'b',		brief = 'Access: b     - Read/write')
	dataField(name = 'u_16_',	type = 'u 16 ',	access = 'B',		brief = 'Access: B     - Read/write')
	dataField(name = 'u_1_6',	type = 'u 1 6',	access = '?',		brief = 'Access: ?     - Undefined')
	dataField(name = 'u_1_6_',	type = 'u 1 6 ',					brief = 'Access: Not set - Undefined')
	endGroup()
	#
	standardCsrRegisters()
	endCsr()
	endFile()


def test_3(testNumber, argv):
	beginFile('Csr_Compiler_Test_' + str(testNumber), 254, (0,1,2), argv = argv, path = 'test_suite',
			brief	= 'Test #' + str(testNumber) + ' of CSR compiler',
			detail	= """
						This is a test of the CSR compiler and is not intended for actual use.
						
						""" + StandardCsrDescription)
	define(' CSR_TEST ', "({})".format(testNumber),
			brief	= ' Test Number ',
			detail	= """
						This value indicates the CSR test number.
						""")
	beginCsr()
	#
	dataField('u16',	'u16',	'RW',	'2-byte unsigned integer')
	#
	beginBits('u32',	'u32',	'RW',	'1-byte unsigned integer')
	bitField('b0',	1,	'B',	'1-bit unsigned integer')
	bitField('b1',	2,	'R',	'2-bit unsigned integer')
	reserveBits(2)
	bitField('b5',	1,	'R',	'1-bit unsigned integer', invert = True)
	setBitOffset(12)
	bitField('b16',	2,	'R',	'2-bit unsigned integer')
	endBits()
	#
	beginBits('U32',	'u32',	'RW',	'1-byte unsigned integer')
	bitField('B0',	1,	'B',	'1-bit unsigned integer')
	bitField('B1',	2,	'R',	'2-bit unsigned integer')
	bitField('B3',	3,	'R',	'3-bit unsigned integer', invert = True)
	bitField('B6',	4,	'R',	'4-bit unsigned integer', invert = True)
	bitField('B10',	5,	'R',	'5-bit unsigned integer')
	bitField('B15',	6,	'R',	'6-bit unsigned integer')
	bitField('B21',	7,	'R',	'7-bit unsigned integer')
	endBits()
	#
	standardCsrRegisters()
	endCsr()
	endFile()

def test_4(testNumber, argv):
	beginFile('Csr_Compiler_Test_' + str(testNumber), 254, (0,1,2), argv = argv, path = 'test_suite',
			brief	= 'Test #' + str(testNumber) + ' of CSR compiler',
			detail	= """
						This is a test of the CSR compiler and is not intended for actual use.
						
						""" + StandardCsrDescription)
	define(' CSR_TEST ', "({})".format(testNumber),
			brief	= ' Test Number ',
			detail	= """
						This value indicates the CSR test number.
						""")
	beginCsr()
	#
	comment()
	comment('Single line C comment.')
	comment()
	#
	comment()
	#
	dataField('u8',		'u8',	'RW',	'1-byte unsigned integer')
	dataField('U8',		'U8',	'RW',	'1-byte unsigned integer')
	dataField('u16',	'u16',	'RW',	'2-byte unsigned integer')
	dataField('U16',	'U16',	'RW',	'2-byte unsigned integer')
	dataField('u32',	'u32',	'RW',	'4-byte unsigned integer')
	dataField('U32',	'U32',	'RW',	'4-byte unsigned integer')
	dataField('u64',	'u64',	'RW',	'8-byte unsigned integer')
	dataField('U64',	'U64',	'RW',	'8-byte unsigned integer')
	dataField('s8',		's8',	'RW',	'1-byte signed integer')
	dataField('S8',		'S8',	'RW',	'1-byte signed integer')
	dataField('s16',	's16',	'RW',	'2-byte signed integer')
	dataField('S16',	'S16',	'RW',	'2-byte signed integer')
	dataField('s32',	's32',	'RW',	'4-byte signed integer')
	dataField('S32',	'S32',	'RW',	'4-byte signed integer')
	dataField('s64',	's64',	'RW',	'8-byte signed integer')
	dataField('S64',	'S64',	'RW',	'8-byte signed integer')
	dataField('f32',	'f32',	'RW',	'4-byte floating point')
	dataField('F32',	'F32',	'RW',	'4-byte floating point')
	dataField('f64',	'f64',	'RW',	'8-byte floating point')
	dataField('F64',	'F64',	'RW',	'8-byte floating point')
	#
	setByteOffset(190)
	reserveBytes(2)
	reserveBytes(2)
	#
	# Duplicate setByteOffset() calls are ignored.
	#
	setByteOffset(200)
	setByteOffset(200)
	#
	verbatim('/*')
	verbatim('This is multi-line C comment generated from multiple verbatim statements.')
	verbatim('*/')
	verbatim(""":/*
				: * This is multi-line C comment.
				: * Second line of the C comment.
				: * Third line of the C comment.
				: * Final line of the C comment.
				: */""")
	verbatim('// This is single line C++ comment.')
	comment()
	dataField('u8_field',	'u8',	'RW',	'1-byte unsigned integer')
	dataField('U8_field',	'U8',	'RW',	'1-byte unsigned integer')
	dataField('u16_field',	'u16',	'RW',	'2-byte unsigned integer')
	dataField('U16_field',	'U16',	'RW',	'2-byte unsigned integer')
	#
	standardCsrRegisters()
	endCsr()
	endFile()


def test_5(testNumber, argv):
	beginFile('Csr_Compiler_Test_' + str(testNumber), 254, (0,1,2), argv = argv, path = 'test_suite',
			brief	= 'Test #' + str(testNumber) + ' of CSR compiler',
			detail	= """
						This is a test of the CSR compiler and is not intended for actual use.
						
						""" + StandardCsrDescription)
	define(' CSR_TEST ', "({})".format(testNumber),
			brief	= ' Test Number ',
			detail	= """
						This value indicates the CSR test number.
						""")
	beginCsr()
	#
	dataField('u8a',	u8,			'RW',	'u8')
	dataField('u8b',	uint8,		'RW',	'uint8')
	dataField('u8c',	uint8_t,	'RW',	'uint8_t')
	dataField('u8d',	quint8,		'RW',	'quint8')
	#
	dataField('u16a',	u16,		'RW',	'u16')
	dataField('u16b',	uint16,		'RW',	'uint16')
	dataField('u16c',	uint16_t,	'RW',	'uint16_t')
	dataField('u16d',	quint16,	'RW',	'quint16')
	#
	dataField('u32a',	u32,		'RW',	'u32')
	dataField('u32b',	uint32,		'RW',	'uint32')
	dataField('u32c',	uint32_t,	'RW',	'uint32_t')
	dataField('u32d',	quint32,	'RW',	'quint32')
	#
	dataField('u64a',	u64,		'RW',	'u64')
	dataField('u64b',	uint64,		'RW',	'uint64')
	dataField('u64c',	uint64_t,	'RW',	'uint64_t')
	dataField('u64d',	quint64,	'RW',	'quint64')
	#
	dataField('s8a',	s8,			'RW',	's8')
	dataField('s8b',	sint8,		'RW',	'sint8')
	dataField('s8c',	int8_t,		'RW',	'int8_t')
	dataField('s8d',	qint8,		'RW',	'qint8')
	#
	dataField('s16a',	s16,		'RW',	's16')
	dataField('s16b',	sint16,		'RW',	'sint16')
	dataField('s16c',	int16_t,	'RW',	'int16_t')
	dataField('s16d',	qint16,		'RW',	'qint16')
	#
	dataField('s32a',	s32,		'RW',	's32')
	dataField('s32b',	sint32,		'RW',	'sint32')
	dataField('s32c',	int32_t,	'RW',	'int32_t')
	dataField('s32d',	qint32,		'RW',	'qint32')
	#
	dataField('s64a',	s64,		'RW',	's64')
	dataField('s64b',	sint64,		'RW',	'sint64')
	dataField('s64c',	int64_t,	'RW',	'int64_t')
	dataField('s64d',	qint64,		'RW',	'qint64')
	#
	dataField('f32a',	f32,		'RW',	'f32')
	dataField('f32b',	float,		'RW',	'float')
	dataField('f64a',	f64,		'RW',	'f64')
	dataField('f64b',	double,		'RW',	'double')
	dataField('f64c',	qreal,		'RW',	'qreal')
	#
	standardCsrRegisters()
	endCsr()
	endFile()

# Test code.
if __name__ == "__main__":
	defaultOptions = sys.argv[1:] + "--outdir ./tst/".split()
	test_1(1, defaultOptions + "--outfile tst1".split())
	test_2(2, defaultOptions + "--outfile tst2".split())
	test_3(3, defaultOptions + "--outfile tst3".split())
	test_4(4, defaultOptions + "--outfile tst4".split())
	test_5(5, defaultOptions + "--outfile tst5".split())
