from csr import *
import sys


def power_converter(argv):
	beginFile('Power_Converter', 0xC3, (1,1,3), argv = argv,
			brief	= 'M5 power converter',
			detail	= StandardCsrDescription)
	beginCsr()
	#
	setByteOffset(238)
	beginGroup('Standard VideoRay device registers')
	dataField('save_settings',	'U16',	'W',	'Code')
	endGroup()
	#
	standardCsrRegisters()
	endCsr()
	endFile()


if __name__ == "__main__":
	power_converter(sys.argv[1:])
