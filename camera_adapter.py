from csr import *
import sys


def camera_adapter(argv):
	beginFile('Camera_Adapter', 0xC4, (1,1,0), argv = argv,
			brief	= 'M5 camera adapter',
			detail	= StandardCsrDescription)
	beginCsr()
	#
	dataField('tilt',						'F32',	'?',	'tilt setting in degrees')
	dataField('pan',						'F32',	'?',	'pan setting in degrees')
	dataField('rotate',						'F32',	'?',	'rotate setting in degrees')
	dataField('focus',						'F32',	'?',	'focus setting in mm')
	dataField('zoom',						'F32',	'?',	'zoom setting in mm')
	dataField('iris',						'F32',	'?',	'iris setting in f-stop')
	reserveBytes(24)
	#
	dataField('camera_command',				'U32',	'?',	'special camera command register')
	reserveBytes(16)
	#
	dataField('internal_pressure',			'F32',	'R',	'internal pressure mBar')
	dataField('internal_temp',				'F32',	'R',	'internal temp C')
	reserveBytes(8)
	#
	dataField('raw_servo_tilt',				'U32',	'R',	'raw servo value')
	dataField('raw_servo_pan',				'U32',	'R',	'raw servo value')
	dataField('raw_servo_rotate',			'U32',	'R',	'raw servo value')
	dataField('raw_servo_focus',			'U32',	'R',	'raw servo value')
	dataField('raw_servo_zoom',				'U32',	'R',	'raw servo value')
	dataField('raw_servo_iris',				'U32',	'R',	'raw servo value')
	reserveBytes(4)
	#
	beginBits('system_flags',				'U8',	'RW',	'Bitfield')
	endBits()
	reserveBytes(3)
	#
	dataField('fault_interlock',			'U32',	'RW',	'password')
	reserveBytes(4)
	#
	dataField('camera_type',				'U8',	'R',	'enum defining camera  if any')
	dataField('encoder_type',				'U8',	'R',	'enum defining encoder if any')
	reserveBytes(2)
	#
	dataField('axis_available_flag',		'U32',	'?',	'servos available')
	reserveBytes(4)
	#
	dataField('servo_tilt_min',				'S16',	'RW',	'minimum travel in degrees')
	dataField('servo_tilt_max',				'S16',	'RW',	'maximum travel in degrees')
	dataField('servo_tilt_min_raw',			'U16',	'?',	'minimum servo value (uS or steps)')
	dataField('servo_tilt_max_raw',			'U16',	'?',	'maximum servo value (uS or steps)')
	dataField('servo_pan_min',				'S16',	'RW',	'minimum travel in degrees')
	dataField('servo_pan_max',				'S16',	'RW',	'maximum travel in degrees')
	dataField('servo_pan_min_raw',			'U16',	'?',	'minimum servo value (uS or steps)')
	dataField('servo_pan_max_raw',			'U16',	'?',	'maximum servo value (uS or steps)')
	dataField('servo_rotate_min',			'S16',	'RW',	'minimum travel in degrees')
	dataField('servo_rotate_max',			'S16',	'RW',	'maximum travel in degrees')
	dataField('servo_rotate_min_raw',		'U16',	'?',	'minimum servo value (uS or steps)')
	dataField('servo_rotate_max_raw',		'U16',	'?',	'maximum servo value (uS or steps)')
	dataField('servo_focus_min',			'S16',	'RW',	'minimum travel in mm')
	dataField('servo_focus_max',			'S16',	'RW',	'maximum travel in mm')
	dataField('servo_focus_min_raw',		'U16',	'?',	'minimum servo value (uS or steps)')
	dataField('servo_focus_max_raw',		'U16',	'?',	'maximum servo value (uS or steps)')
	dataField('servo_zoom_min',				'S16',	'RW',	'minimum travel in mm')
	dataField('servo_zoom_max',				'S16',	'RW',	'maximum travel in mm')
	dataField('servo_zoom_min_raw',			'U16',	'?',	'minimum servo value (uS or steps)')
	dataField('servo_zoom_max_raw',			'U16',	'?',	'maximum servo value (uS or steps)')
	dataField('servo_iris_min',				'F32',	'RW',	'minimum travel in f-stop')
	dataField('servo_iris_max',				'F32',	'RW',	'maximum travel in f-stop')
	dataField('servo_iris_min_raw',			'U16',	'?',	'minimum servo value (uS or steps)')
	dataField('servo_iris_max_raw',			'U16',	'?',	'maximum servo value (uS or steps)')
	reserveBytes(8)
	#
	dataField('fault_control',				'U32',	'RW',	'flag')
	dataField('pressure_max',				'F32',	'?',	'Maximum pressure')
	dataField('temp_max',					'F32',	'?',	'Maximum temperature')
	#
	beginGroup('Internal temperature/pressure error stats')
	dataField('vac_err_cnt',				'U32',	'R',	'count')
	dataField('temp_err_cnt',				'U32',	'R',	'count')
	endGroup()
	#
	beginGroup('Communication error stats')
	dataField('comms_sync1_err_cnt',		'U32',	'R',	'count')
	dataField('comms_sync2_err_cnt',		'U32',	'R',	'count')
	dataField('comms_headerxsum_err_cnt',	'U32',	'R',	'count')
	dataField('comms_overrun_err_cnt',		'U32',	'R',	'count')
	dataField('comms_payloadxsum_err_cnt',	'U32',	'R',	'count')
	dataField('comms_err_flag',				'U16',	'?',	'Communications error flag')
	dataField('save_settings',				'U16',	'W',	'Code')
	endGroup()
	#
	standardCsrRegisters()
	endCsr()
	endFile()


if __name__ == "__main__":
	camera_adapter(sys.argv[1:])
