from csr import *
import sys


def attitude_sensor(argv):
	beginFile('Attitude_Sensor', 0xC2, (0,0,1), argv = argv,
			brief	= 'M5 attitude sensor',
			detail	= StandardCsrDescription)
	beginCsr()
	#
	dataField('any_fault_flag',			'U32',	'R',	'flags')
	dataField('ch1_bw',					'F32',	'R',	'bandwidth bytes per second')
	dataField('ch2_bw',					'F32',	'R',	'bandwidth bytes per second')
	reserveBytes(16)
	#
	dataField('ch1_fault',				'U32',	'R',	'flags')
	dataField('ch1_net_tx_cnt',			'U32',	'R',	'byte count')
	dataField('ch1_net_rx_cnt',			'U32',	'R',	'byte count')
	dataField('ch1_uart_tx_cnt',		'U32',	'R',	'byte count')
	dataField('ch1_uart_rx_cnt',		'U32',	'R',	'byte count')
	dataField('ch2_fault',				'U32',	'R',	'flags')
	dataField('ch2_net_tx_cnt',			'U32',	'R',	'byte count')
	dataField('ch2_net_rx_cnt',			'U32',	'R',	'byte count')
	dataField('ch2_uart_tx_cnt',		'U32',	'R',	'byte count')
	dataField('ch2_uart_rx_cnt',		'U32',	'R',	'byte count')
	reserveBytes(88)
	#
	beginBits('system_flags',			'U8',	'RW',	'Bitfield')
	endBits()
	reserveBytes(3)
	#
	dataField('fault_interlock',		'U32',	'RW',	'password')
	dataField('fault_control',			'U32',	'RW',	'flag')
	reserveBytes(4)
	#
	dataField('ip_address',				'U32',	'RW',	'IP address')
	dataField('ip_netmask',				'U32',	'RW',	'IP net mast')
	dataField('ip_gateway',				'U32',	'RW',	'IP gateway')
	dataField('command_port',			'U16',	'RW',	'Command port')
	reserveBytes(2)
	#
	dataField('ch1_HW_id',				'U8',	'RW',	'ordinal')
	dataField('ch1_type',				'U8',	'RW',	'HW type enum')
	dataField('ch1_port',				'U16',	'RW',	'server port')
	dataField('ch1_speed',				'U32',	'RW',	'baud rate')
	comment()
	dataField('ch2_HW_id',				'U8',	'RW',	'ordinal')
	dataField('ch2_type',				'U8',	'RW',	'HW type enum')
	dataField('ch2_port',				'U16',	'RW',	'server port')
	dataField('ch2_speed',				'U32',	'RW',	'baud rate')
	reserveBytes(16)
	#
	dataField('imu_type',				'U8',	'RW',	'IMU type')
	dataField('depth_senor_type',		'U8',	'RW',	'Depth sensor type')
	dataField('depth_tx_rate',			'U16',	'RW',	'Depth sensor baud rate')
	#
	setByteOffset(238)
	beginGroup('Standard VideoRay device registers')
	dataField('save_settings',			'U16',	'W',	'Code')
	endGroup()
	#
	standardCsrRegisters()
	endCsr()
	endFile()


if __name__ == "__main__":
	attitude_sensor(sys.argv[1:])
