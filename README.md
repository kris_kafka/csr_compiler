# CSR Compiler

This tool allows for the compilation of CSR structures.

Version 0.1.2:
Preliminary pre-release version.

================
Usage
-----
make_all.py [-h | --help] [--outdir outDir] [--outfile outFile]
            [--include incDst][--control ctlDst] [--listing lstDst]
            [--version] [-v | --verbose] [@indirectFile]

CSR Compiler

optional arguments:
  -h, --help         Show the help message and exits.
  --outdir outDir    Set default destination directory.
  --outfile outFile  Set default destination file name.
  --control ctlDst   Set path/file/extension of the generated control file.
  --include incDst   Set path/file/extension of the generated include file.
  --listing lstDst   Set path/file/extension of the generated listing file.
  --version          Show version information.
  -v, --verbose      Increase output verbosity
  @indirectFile      Read options from indirectFile.

Defaults:
  outDir   The current directory.
  outFile  The base file name of the source file.

Path/file/extension:
  Consist of the following three parts:
    Path       Must be at the beginning and end with a slash (or a colon under
               Microsoft Windows).  Consists of everything until and including
               the last slash (or colon under Microsoft Windows).  If missing,
               use the value of the outDir option.

    File name  Does not contain any slashes and must not start with a period.
               If missing, use the value of the outFile option.

    Extension  Must be at the end and must start with a period.  Consists of
               the last period (after all slashes) and everything after it.
               If missing, use the default for the option: ctlDst: ".ctl",
               incDst: ".h", lstDst: ".lst"

Vebosity levels:
  1: Show the devices names being compiled (from the beginFile statements).
  2: Show (control and include) file generation messages.
  3: Show the input statements being processed.
  4: Show include file statement processing messages.

================
Generated Files
---------------
1.  C header
    This file contains the generated CSR structure and may be included by a C
    or C++ source file.

2.  Control
    This file contains a JSON descrition of the CSR generated structure.

3.  Listing
    This file contains the following:
    1.  Listing of the input statemensts.
    2.  The compiler version and date/time of the compilation.
    3.  Ths source file relative path and its last modification date/time.
    4.  The device name, type, version, and description.
    5.  Access bit map. For each bit in the CSR the access mode:
        '.': bit not defined, '?': access mode not defined,
        'R': read only access, 'W': write only access,
        'B': both read and write access.
    6.  Dump of the CSR data: source (or subroutine) line number), CSR offset,
        type of data/field, data/field specific parameters.


================
To Do
-----
1.  Add additional detailed descriptions to the standardCsrRegisters function.

2.  Update the CSR source files.

3.  Test the _makeFilePath function (the 'path/file/extension' handler).

4.  Test the @indirectFile functionality.

5.  Add support for formatting fields (width, decimal digits, prefix character,
    format character, is valid expression) to dataField().
