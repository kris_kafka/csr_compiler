import argparse
import datetime
import inspect
import json
import os
import re
import sys
import time

# CSR compiler version.
_VersionMajor	= 0
_VersionMinor	= 1
_VersionVariant	= 2

########################################################################################################################
# ToDo:
# 1.	Change include file generation comment processing so that brief and detail comments become doxygen comments.
# 2.	Add docstrings.
# 3.	Generate documentation with doxygen.
########################################################################################################################

########################################################################################################################
# Include file formatting samples.
########################################################################################################################
#																		Description				#	Variable
#	 +----------------------------------------------------------------	Line indent					IncLineIndentTxt
#	 |      +---------------------------------------------------------	Data type					IncDataTypeFmt
#	 |      |      +--------------------------------------------------	Field name
#	 |      |      | +------------------------------------------------	Field name suffix			IncNameSuffixTxt
#	 |      |      | |                 +------------------------------	Comment delimiter			IncCommentDelimLen
#	 |      |      | |                 |  +---------------------------	Comment prefix				IncCommentPrefixTxt
#	 |      |      | |                 |  |  +------------------------	Byte offset					IncOffsetFmt
#	 |      |      | |                 |  |  | +----------------------	Bit offset delimiter	1	IncCommentBitDelimTxt
#	 |      |      | |                 |  |  | |+---------------------	Bit offset				1,2	IncBitOffsetFmt
#	 |      |      | |                 |  |  | ||+--------------------	Access code delimiter		IncAccessDelimTxt
#	 |      |      | |                 |  |  | ||| +------------------	Access code					IncAccessFmt
#	 |      |      | |                 |  |  | ||| |       +----------	Brief comment
#	 |      |      | |                 |  |  | ||| |       |       +--	Comment suffix				IncCommentSuffixTxt
#	 |  vvvvvvvvvv | v                 vv | vvv|v|vvv             vvv
#	vvvv          vvv                    vvv   v v   vvvvvvvvvvvvv
#	    uint16_t  u16;                   /*   2.- RW Brief comment */
#																		#1	Blank for non-bit fields.
#																		#2	IncBitOffsetNoneTxt for beginBits data field.
####
#	                +-------------------------------------------------	Bit size delimiter			IncBitSizePrefixTxt
#	                | +-----------------------------------------------	Bit size					IncBitSizeFmt
#	                |vv
#	                v
#	    uint16_t  b0:12;                 /*   2.0 RW Brief comment */
####
#																		Description					Variable
#	               +--------------------------------------------------	Rsv name bit prefix			IncResvBitPrefixTxt
#	               |  +-----------------------------------------------	Rsv name offset				IncOffset0Fmt (IncOffsetLen)
#	               |  |  +--------------------------------------------	Rsv name bit delimiter		IncResvBitDelimTxt
#	               |  |  |+-------------------------------------------	Rsv name bit offset			IncBitSize0Fmt (IncBitSizeLen)
#	               |  |  || +-----------------------------------------	Rsv name bit size prefix	IncBitSizePrefixTxt
#	               |  |  || | +---------------------------------------	Rsv name bit size			IncBitSizeFmt
#	               |  |  || | |+--------------------------------------	Rsv name bit size suffix	IncNameSuffixTxt
#	               |  vvv|vv|vv|
#	              vvvv   v  v  v
#	    uint16_t  res_002_03:12;          /*   2.3 */
####
#																		Description					Variable
#	               +--------------------------------------------------	Rsv name byte prefix		IncResvBytePrefixTxt
#	               |   +----------------------------------------------	Rsv name offset				IncOffset0Fmt (IncOffsetLen)
#	               |   | +--------------------------------------------	Rsv name byte size prefix	IncResvByteSizePrefixTxt
#	               |   | | +------------------------------------------	Rsv name byte size			IncOffsetFmt
#	               |   | | | +----------------------------------------	Rsv name byte size suffix	IncResvByteSizeSuffixTxt
#                  |  vvv|vvv|
#                 vvvv   v   v
#	    char      res_003[235];          /*   3 */
####
#																		Description					Variable
#	     +--------------------------------------------------------		Detail comment prefix
#	     |                                     +------------------		Comment suffix				IncCommentSuffixTxt
#	vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv           vvv
#	                            /* Detail comment
#	                               Other lines */
#	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^	
#	         +--------------------------------------------------------	Detail comment other
####
#																		Description					Variable
#	   +--------------------------------------------------------------	Define prefix				IncDefineTxt
#	   |           +--------------------------------------------------	Define name					
#	   |           |        +-----------------------------------------	Define delimiter			IncDefineDelimLen
#	   |           |        |     +-----------------------------------	Define value				
#	   |           |        |     |    +------------------------------	Comment delimiter			IncCommentDelimLen
#	   |           |        |     |    |  +---------------------------	Comment prefix				IncCommentPrefixTxt
#	   |           |        |     |    |  |       +-------------------	Brief comment
#	   |           |        |     |    |  |       |        +----------	Comment suffix				IncCommentSuffixTxt
#	   |    vvvvvvvvvvvvvvvv| vvvvvvvvv| vvv      |       vvv
#	vvvvvvvv                vv         vv   vvvvvvvvvvvvvv
#	#define UtlCmdReboot      0xdead     /* Detail comment */
########################################################################################################################

# CSR compiler errors.
class CsrCompilerError(Exception):
	# Initialize an exception.
	def __init__(self, location, message):
		self.errorFile, self.errorLine, self.errorStatement = location
		self.args			= (self.errorFile, self.errorLine, self.errorStatement, message)
		self.errorMessage	= message

########################################################################################################################


class _Csr:
	###########################################################################
	# Command line/parameter file argument parser.
	class _CsrArgumentParser(argparse.ArgumentParser):
		# Split an indirect file line into separate fields.
		def convert_arg_line_to_args(self, arg_line):
			return arg_line.split()

	###########################################################################
	# Module constants.
	BitsPerByte			= 8
	NibblesPerByte		= 2
	CsrSize				= 256
	#
	DeviceTypeMaximum	= 254
	DeviceTypeMinimum	= 1
	SubVersionMaximum	= 255
	SubVersionMinimum	= 0

	# Include file formating constants.
	IncMinCommentColumn			= 40
	IncAccessDelimLen			= 1
	IncAccessLen				= 2
	IncBitOffsetLen				= 1
	IncBitSizeLen				= 2
	IncCommentDelimLen			= 2
	IncDataTypeLen				= 10
	IncDefineDelimLen			= 2
	IncLineIndentLen			= 4
	IncOffsetLen				= 3

	IncAccessFmt				= "{:<" + str(IncAccessLen + IncAccessDelimLen) + "}"
	IncBitOffsetFmt				= "{:<" + str(IncBitOffsetLen) + "}"
	IncBitOffsetNoneTxt			= '-' * IncBitOffsetLen
	IncBitSize0Fmt				= "{:0>" + str(IncBitSizeLen) + "}"
	IncBitSizeFmt				= "{:<" + str(IncBitSizeLen) + "}"
	IncDataTypeFmt				= "{:<" + str(IncDataTypeLen) + "}"
	IncOffset0Fmt				= "{:0>" + str(IncOffsetLen) + "}"
	IncOffsetFmt				= "{:>" + str(IncOffsetLen) + "}"

	IncAccessDelimTxt			= " "
	IncBitInvertYes				= "1"
	IncBitInvertNo				= "0"
	IncBitBitsSuffix			= "_BITS"
	IncBitInvertSuffix			= "_INVERT"
	IncBitMask0Suffix			= "_MASK0"
	IncBitMask1Suffix			= "_MASK"
	IncBitShiftSuffix			= "_SHIFT"
	IncBitSizeSuffix			= "_SIZEOF"
	IncBitTrue0Suffix			= "_TRUE0"
	IncBitTrue1Suffix			= "_TRUE"
	IncBitSizePrefixTxt			= ":"
	IncCommentBitDelimTxt		= "."
	IncCommentPrefixTxt			= "/* "
	IncCommentSuffixTxt			= " */"
	IncCommentSuffixEolTxt		= IncCommentSuffixTxt + "\n"
	IncCsrBegin1Fmt				= "typedef struct CSR_{}_tag"
	IncCsrBegin2Txt				= " {\n"
	IncCsrEnd1Txt				= "} "
	IncCsrEnd2Fmt				= "CSR_{};\n"
	IncDataTypeCharTxt			= "char"
	IncDataTypeFloat32Txt		= "float"
	IncDataTypeFloat64Txt		= "double"
	IncDataTypeSignedFmt		= "int{}_t"
	IncDataTypeUnsignedFmt		= "uint{}_t"
	IncDefineTxt				= "#define "
	IncDoxygenCommentPrefixTxt	= "/*!"
	IncGroupBeginTxt			= IncDoxygenCommentPrefixTxt + " @{" + IncCommentSuffixEolTxt
	IncGroupEndTxt				= IncDoxygenCommentPrefixTxt + " @}" + IncCommentSuffixEolTxt
	IncGroupNameFmt				= "@name {}"
	IncGroupNamePrefixTxt		= IncDoxygenCommentPrefixTxt + " "
	IncGroupNameSuffixTxt		= IncCommentSuffixEolTxt
	IncGuardNameFmt				= "{}_CSR_H"
	IncLineIndentTxt			= " " * IncLineIndentLen
	IncNameSuffixTxt			= ";"
	IncResvBitDelimTxt			= "_"
	IncResvBitPrefixTxt			= "res_"
	IncResvBytePrefixTxt		= "res_"
	IncResvByteSizePrefixTxt	= "["
	IncResvByteSizeSuffixTxt	= "]"
	IncStructEndTxt				= "};\n"
	IncStructStartTxt			= "struct {\n"
	IncUnionEndTxt				= "};\n"
	IncUnionStartTxt			= "union {\n"

	IncMaxBitNameSuffixLen		= max(	len(IncBitBitsSuffix),
										len(IncBitInvertSuffix),
										len(IncBitMask0Suffix),
										len(IncBitMask1Suffix),
										len(IncBitShiftSuffix),
										len(IncBitSizeSuffix),
										len(IncBitTrue0Suffix),
										len(IncBitTrue1Suffix))

	IncFileHdrPart1Fmt	= "#ifndef {}\n" \
						+ "#define {}\n" \
						+ "\n" \
						+ IncDoxygenCommentPrefixTxt + "\n" \
						+ "@file   {}/CSR.h\n" \
						+ "@brief  {}\n"
	IncFileHdrPart2Fmt	= "*/\n" \
						+ "\n" \
						+ "/*############################################################################\n" \
						+ "BE SURE TO DOULBE CHECK THE API FUNCTIONS THAT USE CSR ADDRESSES ANYTIME THE\n" \
						+ "CSR STRUCTURE CHANGES\n" \
						+ "############################################################################*/\n" \
						+ "\n" \
						+ "/*\n" \
						+ "Compiler version:  {}\n" \
						+ "Compile time:      {}\n" \
						+ "Source file:       {}\n" \
						+ "Source modified    {}\n" \
						+ "Device name:       {}\n" \
						+ "Device type:       {}\n" \
						+ "Device version:    {}\n"
	IncFileHdrPart3Txt	= "*/\n" \
						+ "\n" \
						+ IncCommentPrefixTxt + "Define standard data types." + IncCommentSuffixEolTxt \
						+ "#include <stdint.h>\n" \
						+ "\n" \
						+ IncCommentPrefixTxt + "Ensure C linkage." + IncCommentSuffixEolTxt \
						+ "#ifdef __cplusplus\n" \
						+ 'extern "C" {\n' \
						+ "#endif\n" \
						+ 	"\n"

	IncFileTlrBitsTxt	= "\n" \
						+ IncCommentPrefixTxt + "Bit field information." + IncCommentSuffixEolTxt
	IncFileTlrTxt		= "\n" \
						+ IncCommentPrefixTxt + "Terminate C linkage." + IncCommentSuffixEolTxt \
						+ "#ifdef __cplusplus\n" \
						+ "}\n" \
						+ "#endif\n" \
						+ "\n" \
						+ "#endif\n"

	IncDataNameFmt				= "{}" + IncNameSuffixTxt
	IncCommentNoBitTxt			= ' '  * (len(IncCommentBitDelimTxt) + IncBitOffsetLen)

	# Statement portion fixed sizes.
	IncResvBitNameLen			= len(IncResvBitPrefixTxt)		+ IncOffsetLen \
								+ len(IncResvBitDelimTxt)		+ IncBitSizeLen \
								+ len(IncBitSizePrefixTxt)		+ IncBitSizeLen \
								+ len(IncNameSuffixTxt)
	IncResvByteNameLen			= len(IncResvBytePrefixTxt)		+ IncOffsetLen \
								+ len(IncResvByteSizePrefixTxt)	+ IncOffsetLen \
								+ len(IncResvByteSizeSuffixTxt)	+ len(IncNameSuffixTxt)

	# Data name formats.
	IncDataFieldNameFmt			= "{}" + IncNameSuffixTxt
	IncBitFieldNameFmt			= "{}" + IncBitSizePrefixTxt + "{}" + IncNameSuffixTxt
	IncResvBitNameFmt			= IncResvBitPrefixTxt \
								+ IncOffset0Fmt \
								+ IncResvBitDelimTxt \
								+ IncBitSize0Fmt \
								+ IncBitSizePrefixTxt \
								+ "{}" \
								+ IncNameSuffixTxt
	IncResvByteNameFmt			= IncResvBytePrefixTxt \
								+ IncOffset0Fmt \
								+ IncResvByteSizePrefixTxt \
								+ "{}" \
								+ IncResvByteSizeSuffixTxt \
								+ IncNameSuffixTxt

	# Minimum name space sizes.
	IncMinDefineNameLen			= 8
	IncMinDefineTextLen			= 8
	IncMinDataNameLen			= max(8,
									(len(IncResvBitPrefixTxt)			+ IncOffsetLen \
										+ len(IncResvBitDelimTxt)		+ IncBitSizeLen \
										+ len(IncBitSizePrefixTxt)		+ IncBitSizeLen \
										+ len(IncNameSuffixTxt)),
									(len(IncResvBytePrefixTxt)			+ IncOffsetLen \
										+ len(IncResvByteSizePrefixTxt)	+ IncOffsetLen \
										+ len(IncResvByteSizeSuffixTxt)	+ len(IncNameSuffixTxt)))

	IncBlankBeforeBeginBits		= False
	IncBlankBeforeBeginCsr		= False
	IncBlankBeforeBeginGroup	= True
	IncBlankBeforeBitField		= False
	IncBlankBeforeComment		= False
	IncBlankBeforeDataFile		= False
	IncBlankBeforeDefine		= False
	IncBlankBeforeEndBits		= False
	IncBlankBeforeEndCsr		= False
	IncBlankBeforeEndGroup		= False
	IncBlankBeforeReserveBits	= False
	IncBlankBeforeReserveBytes	= False

	IncBlankAfterBeginBits		= False
	IncBlankAfterBeginCsr		= False
	IncBlankAfterBeginGroup		= False
	IncBlankAfterBitField		= False
	IncBlankAfterComment		= False
	IncBlankAfterDataField		= False
	IncBlankAfterDefine			= False
	IncBlankAfterEndBits		= False
	IncBlankAfterEndCsr			= False
	IncBlankAfterEndGroup		= True
	IncBlankAfterReserveBits	= False
	IncBlankAfterReserveBytes	= False
	IncBlankAfterVerbatim		= False

	# Listing file formatting constants.
	LstAccessBytesPerLine	= 8
	LstCsrCaptionIndent		= 21
	LstDelimiterSize		= 2
	LstLineSize				= 5
	LstByteOffsetSize		= 3
	LstOffsetSize			= LstByteOffsetSize + 2
	LstTypeSize				= 15
	LstArgumentIndentSize	= LstLineSize + LstOffsetSize + LstTypeSize + 3 * LstDelimiterSize

	LstArgumentIndentText	= ' ' * LstArgumentIndentSize
	LstDelimiterText		= ' ' * LstDelimiterSize
	LstLineFomat			= "{:>" + str(LstLineSize) + "}"
	LstOffsetFmt			= "{:>" + str(LstByteOffsetSize) + "}.{}"
	LstTypeFormat			= "{}"
	LstPrefixFormat			= LstLineFomat \
							+ LstDelimiterText \
							+ "{}" \
							+ LstDelimiterText \
							+ LstTypeFormat

	# Error messages.
	InvalidAccessError			= 'Invalid access (type)'
	InvalidDataTypeError		= 'Invalid data type (type)'
	InvalidDefineTextError		= 'Invalid name (type)'
	InvalidNameError			= 'Invalid name (type)'
	InvalidMultiLineTextError	= 'Invalid multi-line text (type)'
	InvalidSingleLineTextError	= 'Invalid single-line text (type)'

	# Verbose formatting constants.
	IncStatementTraceFormat	= "{}" + LstDelimiterText + LstLineFomat + LstDelimiterText + "{}"

	# Access code maps.
	Access_ToInternal	= {	'B'		: 'B',
							'R'		: 'R',
							'W'		: 'W',
							'RW'	: 'B',
							'WR'	: 'B',
							'?'		: '?',
							' '		: ' '}

	Access_ToExteranal	= {	'B'	: 'RW',
							'R'	: 'R ',
							'W'	: ' W',
							'?'	: '??',
							' '	: '  '}


	# Initialize the object.
	def __init__(self):
		# CSR data.
		self.csrAccess	= ''	# Bit access mode flags.
		self.csrData	= {}	# CSR data.
		self.byteOffset	= 0		# Byte offset.

		# Bit field data.
		self.bitsAccess		= '.'
		self.bitsBitSize	= 0
		self.bitsOffset		= 0
		self.bitsDataSize	= 0
		self.bitsDataType	= 'U'

		# Name lengths.
		self.nameLengthBitData		= 0
		self.nameLengthBitField		= 0
		self.nameLengthDataField	= _Csr.IncMinDataNameLen
		self.nameLengthDefine		= _Csr.IncMinDefineNameLen
		self.textLengthDefine		= _Csr.IncMinDefineTextLen
		#
		self.defineNames	= []
		self.dataNames		= []

		# Status
		self.isInBits	= False
		self.isInCsr	= False
		self.isInFile	= False
		self.isInGroup	= False

		# Source file information.
		self.sourceFile		= ''
		self.sourceLine		= 0
		self.sourceSubr		= 0
		self.statementArgs	= ''
		self.statementError	= ''
		self.statementType	= ''

		# Misc data.
		self.lstFile		= 0
		self.options		= {}
		self.newDevDelim	= ''

		# Include file formatting values.
		self.bitBeginTxt	= ""
		self.bitDefineFmt	= ""
		self.bitDataFmt		= ""
		self.bitFieldFmt	= ""
		self.bitFirstFmt	= ""
		self.bitFirstTxt	= ""
		self.bitLastFmt		= ""
		self.bitLastTxt		= ""
		self.bitPadFmt		= ""
		self.bytePadFmt		= ""
		self.defineFmt		= ""
		self.dataFieldFmt	= ""
		self.needBlankLine	= False
		self.bitDefines		= []


	# Set the access mode of the next size bits to mode.
	# mode: '.'=Unset, 'R'=Read, 'W'=Write, 'B'=Read/Write, '?'=Unknown
	def accessSetNextBits(self, size, mode):
		#
		bitOffset	= _Csr.BitsPerByte * self.byteOffset + self.bitsOffset
		self.csrAccess = self.csrAccess[: bitOffset] \
					+ (mode * size) \
					+ self.csrAccess[bitOffset + size :]


	# Set the access mode of the next size bytes to mode.
	# mode: '.'=Unset, 'R'=Read, 'W'=Write, 'B'=Read/Write, '?'=Unknown
	def accessSetNextBytes(self, size, mode):
		#
		bitOffset	= _Csr.BitsPerByte * self.byteOffset
		bitSize		= _Csr.BitsPerByte * size
		self.csrAccess = self.csrAccess[: bitOffset] \
					+ (mode * bitSize) \
					+ self.csrAccess[bitOffset + bitSize :]


	# Add given data to the CSR.
	def addData(self, type, data = None):
		record = {	'Line'		: self.sourceLineGet(),
					'BitOffset'	: self.byteOffset * _Csr.BitsPerByte + self.bitsOffset,
					'Type'		: type}
		if data :
			record['data'] = data
		self.csrData['self.csrData'].append(record)


	# Set the maximum bit data name length.
	def bitDataNameLengthSet(self, name):
		self.dataNames = self.checkForDuplicate(self.dataNames, name)
		length = len(name)
		if self.nameLengthBitData < length :
			self.nameLengthBitData = length


	# Set the maximum bit field name length.
	def bitFieldNameLengthSet(self, name):
		self.dataNames = self.checkForDuplicate(self.dataNames, name)
		length = len(name)
		if self.nameLengthBitField < length :
			self.nameLengthBitField = length


	# Initialize the bit field values.
	def bitsInit(self):
		self.bitsAccess		= '.'
		self.bitsBitSize	= 0
		self.bitsOffset		= 0
		self.bitsDataSize	= 0
		self.bitsDataType	= 'U'


	# Check access codes.
	def checkAccess(self, code):
		if not code :
			raise CsrCompilerError(self.errorLocation(), 'Missing access code')
		try:
			if _Csr.InvalidAccessError == code :
				raise CsrCompilerError(self.errorLocation(), 'Invalid access code (type)')
			isOk	=  (code == '?') \
					or (code == 'B') \
					or (code == 'R') \
					or (code == 'W') \
					or (code == 'RW') \
					or (code == 'WR')
			if not isOk :
				raise CsrCompilerError(self.errorLocation(), 'Invalid access code')
		except TypeError:
			raise CsrCompilerError(self.errorLocation(), 'Invalid access code (type)')


	# Check a bit offset.
	def checkBitOffset(self, bitOffset):
		try:
			if bitOffset < 0 or self.bitsBitSize <= bitOffset :
				raise CsrCompilerError(self.errorLocation(),
										'Invalid bit offset (range [0...{}))'.format(self.bitsBitSize))
			if bitOffset < self.bitsOffset :
				raise CsrCompilerError(self.errorLocation(), 'Invalid bit offset (backwards)')
		except TypeError:
			raise CsrCompilerError(self.errorLocation(), 'Invalid bit offset (type)')


	# Check a byte offset.
	def checkByteOffset(self, byteOffset):
		try:
			if byteOffset < 0 or _Csr.CsrSize <= byteOffset :
				raise CsrCompilerError(self.errorLocation(),
										'Invalid byte offset (range [0...{}})'.format(_Csr.CsrSize))
			if byteOffset < self.byteOffset :
				raise CsrCompilerError(self.errorLocation(), 'Invalid byte offset (backwards)')
		except TypeError:
			raise CsrCompilerError(self.errorLocation(), 'Invalid byte offset (type)')


	# Check a bit size parameter.
	def checkBitSize(self, size):
		try:
			if size < 0 or self.bitsBitSize < size :
				raise CsrCompilerError(self.errorLocation(),
										'Invalid bit size (range: [0...{}])'.format(self.bitsBitSize))
			if self.bitsOffset + size > self.bitsBitSize :
				raise CsrCompilerError(self.errorLocation(), 'Invalid bit size (overflow)')
		except TypeError:
			raise CsrCompilerError(self.errorLocation(), 'Invalid bit size (type)')


	# Check data field types.
	def checkDataType(self, type):
		if not type :
			raise CsrCompilerError(self.errorLocation(), 'Missing data type')
		try:
			if _Csr.InvalidDataTypeError == type :
				raise CsrCompilerError(self.errorLocation(), 'Invalid data type (type)')
			elif re.fullmatch(r'(?:[US](?:8|16|32|64))|(?:F(?:32|64))', type) :
				return type
			elif type == 'UINT8' or type == 'UINT8_t' or type == 'QUINT8' :
				return 'U8'
			elif type == 'UINT16' or type == 'UINT16_t' or type == 'QUINT16' :
				return 'U16'
			elif type == 'UINT32' or type == 'UINT32_t' or type == 'QUINT32' :
				return 'U32'
			elif type == 'UINT64' or type == 'UINT64_t' or type == 'QUINT64' :
				return 'U64'
			elif type == 'SINT8' or type == 'INT8_t' or type == 'QINT8' :
				return 'S8'
			elif type == 'SINT16' or type == 'INT16_t' or type == 'QINT16' :
				return 'S16'
			elif type == 'SINT32' or type == 'INT32_t' or type == 'QINT32' :
				return 'S32'
			elif type == 'SINT64' or type == 'INT64_t' or type == 'QINT64' :
				return 'S64'
			elif type == 'FLOAT' :
				return 'F32'
			elif type == 'DOUBLE' or type == 'QREAL' :
				return 'F64'
			raise CsrCompilerError(self.errorLocation(), 'Invalid data type')
		except TypeError:
			raise CsrCompilerError(self.errorLocation(), 'Invalid data type (type)')

	# Check a define text.
	def checkDefineText(self, text):
		if not text :
			raise CsrCompilerError(self.errorLocation(), 'Missing define text')
		if _Csr.InvalidDefineTextError == text :
			raise CsrCompilerError(self.errorLocation(), 'Invalid define text (type)')

	# Check device types.
	def checkDeviceType(self, type):
		if not type :
			raise CsrCompilerError(self.errorLocation(), 'Missing device type')
		try:
			isBad = type < _Csr.DeviceTypeMinimum \
				or _Csr.DeviceTypeMaximum < type
			if isBad :
				raise CsrCompilerError(self.errorLocation(),
										'Invalid device type (range [{}...{}])'
										.format(_Csr.DeviceTypeMinimum, _Csr.DeviceTypeMaximum))
		except TypeError:
			raise CsrCompilerError(self.errorLocation(), 'Invalid device type (type)')


	# Check a device sub-version field.
	def checkDeviceSubVersion(self, value):
		# TypeError errors are caught by the caller.
		isBad = value < _Csr.SubVersionMinimum \
				or _Csr.SubVersionMaximum < value \
				or 0 != value % 1
		return not isBad


	# Check a device version.
	def checkDeviceVersion(self, version):
		if not version :
			raise CsrCompilerError(self.errorLocation(), 'Missing device version')
		try:
			major, minor, build = version
			if not self.checkDeviceSubVersion(major):
				raise CsrCompilerError(self.errorLocation(), 'Invalid device version (major)')
			if not self.checkDeviceSubVersion(minor):
				raise CsrCompilerError(self.errorLocation(), 'Invalid device version (minor)')
			if not self.checkDeviceSubVersion(build):
				raise CsrCompilerError(self.errorLocation(), 'Invalid device version (build)')
		except TypeError:
			raise CsrCompilerError(self.errorLocation(), 'Invalid device version (type)')


	# Check for duplicate names.
	def checkForDuplicate(self, list, name):
		if name in list :
			raise CsrCompilerError(self.errorLocation(), 'Duplicate name')
		list.append(name)
		return list


	# Check a single single line text parameter.
	def checkMultiLineText(self, text):
		if not text :
			return
		if _Csr.InvalidMultiLineTextError == text :
			raise CsrCompilerError(self.errorLocation(), 'Invalid multi-line text (type)')


	# Check a name.
	def checkName(self, name):
		if not name :
			raise CsrCompilerError(self.errorLocation(), 'Missing name')
		try:
			if _Csr.InvalidNameError == name :
				raise CsrCompilerError(self.errorLocation(), 'Invalid name (type)')
			if not re.fullmatch(r'[_A-Za-z][_A-Za-z0-9]*', name) :
				raise CsrCompilerError(self.errorLocation(), 'Invalid name')
		except TypeError:
			raise CsrCompilerError(self.errorLocation(), 'Invalid name (type)')


	# Check a bit reservation parameter.
	def checkReserveBits(self, size):
		try:
			if size < 0 or self.bitsBitSize < size :
				raise CsrCompilerError(self.errorLocation(),
										'Invalid bit reservation size (range: [0...{}])'.format(self.bitsBitSize))
			if self.bitsOffset + size > self.bitsBitSize :
				raise CsrCompilerError(self.errorLocation(), 'Invalid bit reservation size (overflow)')
		except TypeError:
			raise CsrCompilerError(self.errorLocation(), 'Invalid bit reservation size (type)')


	# Check a byte reservation parameter.
	def checkReserveBytes(self, size):
		try:
			if size < 0 or _Csr.CsrSize <= size :
				raise CsrCompilerError(self.errorLocation(),
										'Invalid byte reservation size (range [0...{}))'.format(_Csr.CsrSize))
			if self.byteOffset + size >= _Csr.CsrSize :
				raise CsrCompilerError(self.errorLocation(), 'Invalid byte reservation size (overflow)')
		except TypeError:
			raise CsrCompilerError(self.errorLocation(), 'Invalid byte reservation size (type)')


	# Check a single single line text parameter.
	def checkSingleLineText(self, text, name):
		if not text :
			raise CsrCompilerError(self.errorLocation(), 'Missing ' + name)
		try:
			if 0 == len('' + text) :
				raise CsrCompilerError(self.errorLocation(), 'Empty ' + name)
			if _Csr.InvalidSingleLineTextError == text :
				raise CsrCompilerError(self.errorLocation(), 'Invalid (type) ' + name)
			if '\n' in text :
				raise CsrCompilerError(self.errorLocation(), 'Invalid (multiple lines) ' + name)
		except TypeError:
			raise CsrCompilerError(self.errorLocation(), 'Invalid (type) ' + name)


	# Clean up an access codes.
	def cleanAccess(self, code):
		if not code :
			return None
		try:
			return code.replace(' ', '').upper()
		except TypeError:
			return _Csr.InvalidAccessError


	# Clean up a data field types.
	def cleanDataType(self, type):
		try:
			return type.replace(' ', '').upper() if type else None
		except TypeError:
			return _Csr.InvalidDataTypeError


	# Clean up a data field types.
	def cleanDefineText(self, text):
		try:
			return str(text).strip() if text is not None else None
		except TypeError:
			return _Csr.InvalidDefineTextError


	# Clean a boolean argument.
	def cleanBoolean(self, value):
		return not not value


	# Clean up a detailed description text.
	def cleanMultiLineText(self, text):
		# Done if not defined.
		if not text :
			return None
		try:
			txt = text.strip()
			# Convert lines consisting of only white space.
			txt = re.sub(r'^[ \t]+$', r'', txt, flags = re.MULTILINE)
			# Remove optional whitespace and colon at the begining of lines.
			txt = re.sub(r'\n[ \t]+([^ \t:])', r'\n\1', txt)
			# Remove optional whitespace and colon at the begining of lines.
			return re.sub(r'^[ \t]*:', r'', txt, flags = re.MULTILINE)
		except TypeError:
			return _Csr.InvalidMultiLineTextError


	# Clean up a name.
	def cleanName(self, name):
		try:
			return name.replace(' ', '') if name else None
		except TypeError:
			return _Csr.InvalidNameError


	# Clean up a brief description text.
	def cleanSingleLineText(self, text):
		try:
			return text.strip() if text else None
		except TypeError:
			return _Csr.InvalidSingleLineTextError


	# Get the compiler version as a tuple.
	def compilerVersion(self):
		global _VersionVariant, _VersionMajor, _VersionMinor
		return {'Major'		: _VersionMajor,
				'Minor'		: _VersionMinor,
				'Variant'	: _VersionVariant}


	# Return the loction of the error.
	def errorLocation(self):
		return self.sourceFile, self.sourceLine, self.statementError


	# Record the CSR data in the listing file - beginBits record.
	def fileList_CsrData_BeginBits(self, text, record):
		data = record['data']
		text = text.ljust(_Csr.LstArgumentIndentSize) \
			+ data['Type'] \
			+ str(data['Size']) \
			+ " " \
			+ data['Access'] \
			+ "  " \
			+ data['Name'] \
			+ "\n" \
			+ _Csr.LstArgumentIndentText \
			+ "'" \
			+ data['Brief'] \
			+ "'"
		if 'Detail' in data :
			text += self.formatTextCsr(data['Detail'],)
		else:
			text += "\n"
		return text


	# Record the CSR data in the listing file - beginCsr record.
	def fileList_CsrData_BeginCsr(self, text, record):
		text += "\n"
		return text


	# Record the CSR data in the listing file - beginGroup record.
	def fileList_CsrData_BeginGroup(self, text, record):
		data = record['data']
		text = text.ljust(_Csr.LstArgumentIndentSize) + data['Name']
		if 'Detail' in data :
			text += self.formatTextCsr(data['Detail'])
		else:
			text += "\n"
		return text


	# Record the CSR data in the listing file - bitField record.
	def fileList_CsrData_BitField(self, text, record):
		data = record['data']
		text = text.ljust(_Csr.LstArgumentIndentSize) \
			+ str(data['Size']) \
			+ " " \
			+ data['Access'] \
			+ "  " \
			+ ("Neg" if data['Invert'] else "Pos") \
			+ "  " \
			+ data['Name'] \
			+ "\n" \
			+ _Csr.LstArgumentIndentText + \
			"'" + data['Brief'] + "'"
		if 'Detail' in data :
			text += self.formatTextCsr(data['Detail'],)
		else:
			text += "\n"
		return text


	# Record the CSR data in the listing file - comment record.
	def fileList_CsrData_Comment(self, text, record):
		if 'data' in record :
			data = record['data']
			text = text.ljust(_Csr.LstArgumentIndentSize) \
				+ self.formatText(data['Text'],
									delimiter = '"',
									other = r'\n' + (' ' * (1 + _Csr.LstArgumentIndentSize)))
		else:
			text += "\n"
		return text


	# Record the CSR data in the listing file - define record.
	def fileList_CsrData_Define(self, text, record):
		data = record['data']
		text = text.ljust(_Csr.LstArgumentIndentSize) \
			+ data['Name'] \
			+ "\n" \
			+ _Csr.LstArgumentIndentText \
			+ data['Text'] \
			+ "\n" \
			+ _Csr.LstArgumentIndentText \
			+ "'" \
			+ data['Brief'] \
			+ "'"
		if 'Detail' in data :
			text += self.formatTextCsr(data['Detail'])
		else:
			text += "\n"
		return text


	# Record the CSR data in the listing file - dataField record.
	def fileList_CsrData_DataField(self, text, record):
		data = record['data']
		text = text.ljust(_Csr.LstArgumentIndentSize) \
			+ data['Type'] \
			+ str(data['Size']) \
			+ " " \
			+ data['Access'] \
			+ "  " \
			+ data['Name'] \
			+ "\n" \
			+ _Csr.LstArgumentIndentText \
			+ "'" \
			+ data['Brief'] \
			+ "'"
		if 'Detail' in data :
			text += self.formatTextCsr(data['Detail'],)
		else:
			text += "\n"
		return text


	# Record the CSR data in the listing file - endBits record.
	def fileList_CsrData_EndBits(self, text, record):
		text += "\n"
		return text


	# Record the CSR data in the listing file - endCsr record.
	def fileList_CsrData_EndCsr(self, text, record):
		text += "\n"
		return text


	# Record the CSR data in the listing file - endGroup record.
	def fileList_CsrData_EndGroup(self, text, record):
		text += "\n"
		return text


	# Record the CSR data in the listing file - reserveBits record.
	def fileList_CsrData_ReserveBits(self, text, record):
		data = record['data']
		text = text.ljust(_Csr.LstArgumentIndentSize) + str(data['Size']) + "\n"
		return text


	# Record the CSR data in the listing file - reserveBytes record.
	def fileList_CsrData_ReserveBytes(self, text, record):
		data = record['data']
		text = text.ljust(_Csr.LstArgumentIndentSize) + str(data['Size']) + "\n"
		return text


	# Record the CSR data in the listing file - verbatim record.
	def fileList_CsrData_Verbatim(self, text, record):
		data = record['data']
		text =	text.ljust(_Csr.LstArgumentIndentSize) \
			+ self.formatText(data['Text'],
								delimiter = '"',
								other = r'\n' + (' ' * (1 + _Csr.LstArgumentIndentSize)))
		return text


	# Record the CSR data in the listing file.
	def fileList_CsrData(self):
		text =	"\fCSR Data:\n"	\
			+	"Line".ljust(_Csr.LstLineSize)	+ _Csr.LstDelimiterText \
			+	"Off".ljust(_Csr.LstOffsetSize)	+ _Csr.LstDelimiterText \
			+	"Type".ljust(_Csr.LstTypeSize)	+ _Csr.LstDelimiterText \
			+	"Parameters\n" \
			+	'-' * _Csr.LstLineSize		+ _Csr.LstDelimiterText \
			+	'-' * _Csr.LstOffsetSize	+ _Csr.LstDelimiterText \
			+	'-' * _Csr.LstTypeSize		+ _Csr.LstDelimiterText \
			+	'-' * (80 - _Csr.LstArgumentIndentSize)
		print(text, file = self.lstFile)
		for record in self.getItem('self.csrData') :
			type = record['Type']
			text = _Csr.LstPrefixFormat.format(record['Line'], self.formatBitOffset(record['BitOffset']), type)
			if 'beginBits' == type :
				text = self.fileList_CsrData_BeginBits(text, record)
			elif 'beginCsr' == type :
				text = self.fileList_CsrData_BeginCsr(text, record)
			elif 'beginGroup' == type :
				text = self.fileList_CsrData_BeginGroup(text, record)
			elif 'bitField' == type :
				text = self.fileList_CsrData_BitField(text, record)
			elif 'comment' == type :
				text = self.fileList_CsrData_Comment(text, record)
			elif 'define' == type :
				text = self.fileList_CsrData_Define(text, record)
			elif 'endBits' == type :
				text = self.fileList_CsrData_EndBits(text, record)
			elif 'endCsr' == type :
				text = self.fileList_CsrData_EndCsr(text, record)
			elif 'endGroup' == type :
				text = self.fileList_CsrData_EndGroup(text, record)
			elif 'dataField' == type :
				text = self.fileList_CsrData_DataField(text, record)
			elif 'reserveBits' == type :
				text = self.fileList_CsrData_ReserveBits(text, record)
			elif 'reserveBytes' == type :
				text = self.fileList_CsrData_ReserveBytes(text, record)
			elif 'verbatim' == type :
				text = self.fileList_CsrData_Verbatim(text, record)
			elif 'data' in record:
				text = text.ljust(_Csr.LstArgumentIndentSize) + str(record['data']) + "\n"
			else:
				text += "\n"
			print(text, end = '', file = self.lstFile)


	# Record the CSR information in the listing file.
	def fileList_CsrInformation(self):
		text = "\fCSR Information:\n" \
			+	"Compiler version:"	.ljust(_Csr.LstCsrCaptionIndent) \
									+ self.formatVersion(self.getItem('CompilerVersion')) + '\n' \
			+	"Compile time:"		.ljust(_Csr.LstCsrCaptionIndent) \
									+ self.getItem('CompileTime') + '\n' \
			+	"Source file:"		.ljust(_Csr.LstCsrCaptionIndent) \
									+ self.getItem('SourceFile') + '\n' \
			+	"Source modified:"	.ljust(_Csr.LstCsrCaptionIndent) \
									+ self.getItem('SourceModified') + '\n' \
			+	"Device name:"		.ljust(_Csr.LstCsrCaptionIndent) \
									+ self.getItem('DeviceName') + '\n' \
			+	"Device type:"		.ljust(_Csr.LstCsrCaptionIndent) \
									+ str(self.getItem('DeviceType')) + '\n' \
			+	"Device version:"	.ljust(_Csr.LstCsrCaptionIndent) \
									+ self.formatVersion(self.getItem('DeviceVersion')) + '\n' \
			+	"Brief description:".ljust(_Csr.LstCsrCaptionIndent) \
									+ self.getItem('CsrBrief') + '\n'
		detail = self.getDetail()
		if detail :
			text +=	"Detail description:".ljust(_Csr.LstCsrCaptionIndent) \
				+	self.formatText(detail,
									prefix = '',
									other = r'\n' + (' ' * _Csr.LstCsrCaptionIndent))
		print(text, end = '', file = self.lstFile)


	# Record the CSR defined bits information in the listing file.
	def fileList_DefinedBits(self):
		text = "\n\nAccess:\n"
		delta = _Csr.LstAccessBytesPerLine
		index = 0
		while index < _Csr.CsrSize :
			end = index + delta
			if end > _Csr.CsrSize :
				end = _Csr.CsrSize
			text += "{:>3}:".format(index)
			while index < end :
				idx = _Csr.BitsPerByte * index
				text += " {}".format(self.csrAccess[idx : _Csr.BitsPerByte + idx])
				index += 1
			text += '\n'
		print(text, end = '', file = self.lstFile)


	# Close the listing file.
	def fileLstClose(self):
		self.lstFile.close()


	# Create and open the listing file.
	def fileLstOpen(self):
		self.lstFile = open(self.makeFilePath('.lst', self.options.lstDst), 'w')
		#
		text =	"Statments:\n" \
			+	"Line".ljust(_Csr.LstLineSize)	+ _Csr.LstDelimiterText \
			+	"Off".ljust(_Csr.LstOffsetSize)	+ _Csr.LstDelimiterText \
			+	"Type".ljust(_Csr.LstTypeSize)	+ _Csr.LstDelimiterText \
			+	"Parameters\n" \
			+	'-' * _Csr.LstLineSize		+ _Csr.LstDelimiterText \
			+	'-' * _Csr.LstOffsetSize	+ _Csr.LstDelimiterText \
			+	'-' * _Csr.LstTypeSize		+ _Csr.LstDelimiterText \
			+	'-' * (80 - _Csr.LstArgumentIndentSize)
		print(text, file = self.lstFile)


	# Record the CSR information in the listing file.
	def fileListRecordCsrData(self):
		if not self.lstFile or self.lstFile.closed :
			return
		#
		self.fileList_CsrInformation()
		self.fileList_DefinedBits()
		self.fileList_CsrData()


	# Record the current statement.
	def fileListRecordStatement(self, type, args = ''):
		text = _Csr.LstPrefixFormat.format(self.sourceLineGet(),
											self.formatByteBitOffset(self.byteOffset, self.bitsOffset),
											type)
		if args :
			text = text.ljust(_Csr.LstArgumentIndentSize) + args
		if self.verbosityShowInputStatements() :
			print(text)
		if self.lstFile and not self.lstFile.closed :
			print(text, file = self.lstFile)


	# Format a bit offset.
	def formatBitOffset(self, bitOffset):
		return _Csr.LstOffsetFmt.format(int(bitOffset / _Csr.BitsPerByte), int(bitOffset % _Csr.BitsPerByte))


	# Format a bit offset.
	def formatByteBitOffset(self, byteOffset, bitOffset):
		return _Csr.LstOffsetFmt.format(self.normalizeByteOffset(byteOffset, bitOffset),
										self.normalizeBitOffset(bitOffset))


	# Format a text argument.
	def formatText(self, text, prefix = '', indent = 0, caption = '', delimiter = '', other = '', suffix = '\n'):
		if not text :
			return ''
		if indent :
			repl = other + ' ' * (indent + len(caption) + len(delimiter))
		else:
			repl = other
		return prefix \
				+ (' ' * indent) \
				+ caption \
				+ delimiter \
				+ re.sub(r'\n', repl, text) \
				+ delimiter \
				+ suffix


	# Format a text argument for statement tracing.
	def formatTextArg(self, text, caption = ''):
		return self.formatText(text,
								prefix = ',\n',
								other = r'\n',
								delimiter = '"',
								suffix = '',
								caption = caption,
								indent = _Csr.LstArgumentIndentSize)


	# Format a text argument for CSR data listing.
	def formatTextCsr(self, text, prefix = ',\n', other = r'\n', indent = -1):
		if 0 > indent :
			indent = _Csr.LstArgumentIndentSize
		return self.formatText(text,
								other = other,
								delimiter = '"',
								prefix = prefix,
								indent = indent)


	# Format the data type and size.
	def formatTypeSize(self, type, size):
		if 'U' == type :
			return _Csr.IncDataTypeUnsignedFmt.format(_Csr.BitsPerByte * size)
		elif 'S' == type :
			return _Csr.IncDataTypeSignedFmt.format(_Csr.BitsPerByte * size)
		elif 4 == size :
			return _Csr.IncDataTypeFloat32Txt
		else:
			return _Csr.IncDataTypeFloat64Txt


	# Format a version.
	def formatVersion(self, version):
		return "{}.{}.{}".format(version['Major'], version['Minor'], version['Variant'])


	# Generate the control file.
	def generateControlFile(self):
		if self.verbosityShowOutputFiles() :
			print("Generating control file:")
		#
		# Generate the control file.
		ctlPath = self.makeFilePath('.ctl', self.options.ctlDst)
		ctlFile = open(ctlPath, 'w')
		json.dump(self.csrData, ctlFile, indent = '\t', sort_keys = True, separators = (',', ': '))
		ctlFile.close()


	# Generate the include file.
	def generateIncludeFile(self):
		if self.verbosityShowOutputFiles() :
			print("Generating include file:")
		#
		self.generateIncludeFile_initialize()
		incPath = self.makeFilePath('.h', self.options.incDst)
		incFile = open(incPath, 'w')
		#
		text = self.generateIncludeFile_fileHeader()
		print(text, end = '', file = incFile)
		#
		self.needBlankLine = False
		for record in self.getItem('self.csrData') :
			type = record['Type']
			if self.verbosityShowIncludeStatements() :
				print(_Csr.IncStatementTraceFormat.format(record['BitOffset'],
															self.formatBitOffset(record['Line']),
															type))
			if 'beginBits' == type :
				text = self.generateIncludeFile_BeginBits(record)
			elif 'beginCsr' == type :
				text = self.generateIncludeFile_BeginCsr(record)
			elif 'beginGroup' == type :
				text = self.generateIncludeFile_BeginGroup(record)
			elif 'bitField' == type :
				text = self.generateIncludeFile_BitField(record)
			elif 'comment' == type :
				text = self.generateIncludeFile_Comment(record)
			elif 'dataField' == type :
				text = self.generateIncludeFile_DataField(record)
			elif 'define' == type :
				text = self.generateIncludeFile_Define(record)
			elif 'endBits' == type :
				text = self.generateIncludeFile_EndBits(record)
			elif 'endCsr' == type :
				text = self.generateIncludeFile_EndCsr(record)
			elif 'endGroup' == type :
				text = self.generateIncludeFile_EndGroup(record)
			elif 'reserveBits' == type :
				text = self.generateIncludeFile_ReserveBits(record)
			elif 'reserveBytes' == type :
				text = self.generateIncludeFile_ReserveBytes(record)
			elif 'verbatim' == type :
				text = self.generateIncludeFile_Verbatim(record)
			else:
				raise CsrCompilerError(self.errorLocation(), 'Internal error (unexpected CSR data): ' + str(record))
			#elif 'data' in record :
			#	text += "{}\n".format(str(record['data']))
			print(text, end = '', file = incFile)
		#
		text = self.generateIncludeFile_bitDefines()
		print(text, end='', file = incFile)
		text = self.generateIncludeFile_fileTrailer()
		print(text, end='', file = incFile)
		#
		incFile.close()


	# Generate include file beginBits statement text.
	def generateIncludeFile_BeginBits(self, record):
		data				= record['data']
		byteOffset			= int(record['BitOffset'] / _Csr.BitsPerByte)
		name				= data['Name']
		brief				= data['Brief']
		detail				= data['Detail'] if 'Detail' in data else None
		self.bitsOffset		= 0
		self.bitsDataSize	= data['Size']
		self.bitsBitSize	= self.bitsDataSize * _Csr.BitsPerByte
		self.bitsDataType	= data['Type']
		self.bitsAccess		= data['Access']
		#
		text	= ("\n" if self.needBlankLine or _Csr.IncBlankBeforeBeginBits else "") \
				+ self.bitBeginTxt \
				+ self.bitDataFmt.format(self.formatTypeSize(self.bitsDataType, self.bitsDataSize),
											_Csr.IncDataFieldNameFmt.format(name),
											byteOffset,
											_Csr.Access_ToExteranal[self.bitsAccess],
											brief) \
				+ self.formatText(detail,
									prefix = self.detailPrefix,
									other = self.detailOther,
									suffix = self.detailSufix)
		self.bitFirstTxt = self.bitFirstFmt
		self.bitLastTxt = ""
		self.needBlankLine = _Csr.IncBlankAfterBeginBits
		return text


	# Generate include file beginCsr statement text.
	def generateIncludeFile_BeginCsr(self, record):
		text = ("\n" if _Csr.IncBlankBeforeBeginCsr else "") \
				+ _Csr.IncCsrBegin1Fmt.format(self.getItem('DeviceName')) \
				+ _Csr.IncCsrBegin2Txt
		self.needBlankLine = _Csr.IncBlankAfterBeginCsr
		return text


	# Generate include file beginGroup statement text.
	def generateIncludeFile_BeginGroup(self, record):
		data = record['data']
		#
		namePrefix		= _Csr.IncLineIndentTxt + _Csr.IncGroupNamePrefixTxt
		detailPrefix	= '\n' + (' ' * len(namePrefix))
		text	= ("\n" if self.needBlankLine or _Csr.IncBlankBeforeBeginGroup else "") \
				+ namePrefix \
				+ _Csr.IncGroupNameFmt.format(data['Name'])
		if 'Detail' in data :
			text += self.formatText(data['Detail'],
									prefix = detailPrefix,
									other = detailPrefix,
									suffix = '')
		text	+=	_Csr.IncGroupNameSuffixTxt \
				+	_Csr.IncLineIndentTxt \
				+	_Csr.IncGroupBeginTxt
		self.needBlankLine	= _Csr.IncBlankAfterBeginGroup
		return text


	# Generate include file bit field statement text.
	def generateIncludeFile_BitField(self, record):
		data		= record['data']
		byteOffset	= self.byteOffset
		#
		name		= data['Name']
		bitOffset	= self.bitsOffset
		invert		= data['Invert']
		access		= data['Access']
		size		= data['Size']
		brief		= data['Brief']
		detail		= data['Detail'] if 'Detail' in data else None
		#
		text	= ("\n" if self.needBlankLine or _Csr.IncBlankBeforeBitField else "") \
				+ self.bitFirstTxt \
				+ self.bitFieldFmt.format(self.formatTypeSize(self.bitsDataType, self.bitsDataSize),
											_Csr.IncBitFieldNameFmt.format(name, size),
											self.normalizeByteOffset(byteOffset, bitOffset),
											self.normalizeBitOffset(bitOffset),
											_Csr.Access_ToExteranal[access],
											brief) \
				+ self.formatText(detail,
									prefix = self.detailPrefix,
									other = self.detailOther,
									suffix = self.detailSufix)
		self.bitsOffset += size
		self.bitDefines.append((name, self.bitsDataSize, bitOffset, size, invert))
		self.bitFirstTxt = ""
		self.bitLastTxt = self.bitLastFmt
		self.needBlankLine = _Csr.IncBlankAfterBitField
		return text


	# Generate include file comment statement text.
	def generateIncludeFile_Comment(self, record):
		if 'data' in record :
			data	= record['data']
			txt		= data['Text']
			text	= ("\n" if self.needBlankLine or _Csr.IncBlankBeforeComment else "") \
					+ self.formatText(txt,
										prefix = _Csr.IncCommentPrefixTxt,
										other = "\n" + (' ' * len(_Csr.IncCommentPrefixTxt)),
										suffix = _Csr.IncCommentSuffixEolTxt)
		else:
			text	= "\n"
		self.needBlankLine = _Csr.IncBlankAfterComment
		return text

	# Generate include file data field statement text.
	def generateIncludeFile_DataField(self, record):
		data		= record['data']
		byteOffset	= int(record['BitOffset'] / _Csr.BitsPerByte)
		name		= data['Name']
		type		= data['Type']
		size		= data['Size']
		access		= data['Access']
		brief		= data['Brief']
		detail		= data['Detail'] if 'Detail' in data else None
		#
		text	= ("\n" if self.needBlankLine or _Csr.IncBlankBeforeDataFile else "") \
				+ self.dataFieldFmt.format(self.formatTypeSize(type, size),
												_Csr.IncDataNameFmt.format(name),
												byteOffset,
												_Csr.Access_ToExteranal[access],
												brief) \
				+ self.formatText(detail,
									prefix = self.detailPrefix,
									other = self.detailOther,
									suffix = self.detailSufix)
		self.byteOffset += size
		self.needBlankLine = _Csr.IncBlankAfterDataField
		return text


	# Generate include file define statement text.
	def generateIncludeFile_Define(self, record):
		data = record['data']
		#
		text	= ("\n" if self.needBlankLine or _Csr.IncBlankBeforeDefine else "") \
				+ self.defineFmt.format(data['Name'], data['Text'], data['Brief'])
		if 'Detail' in data :
			text += self.formatText(data['Detail'],
									prefix = self.detailPrefix,
									other = self.detailOther,
									suffix = self.detailSufix)
		self.needBlankLine = _Csr.IncBlankAfterDefine
		return text


	# Generate include file endBits statement text.
	def generateIncludeFile_EndBits(self, record):
		text	= ("\n" if _Csr.IncBlankBeforeEndBits else "") \
				+ self.bitLastTxt \
				+ _Csr.IncLineIndentTxt + _Csr.IncUnionEndTxt
		self.byteOffset += self.bitsDataSize
		self.bitsInit()
		self.bitFirstTxt = self.bitFirstFmt
		self.bitLastTxt = ""
		self.needBlankLine = _Csr.IncBlankAfterEndBits
		return text


	# Generate include file endCsr statement text.
	def generateIncludeFile_EndCsr(self, record):
		text	= ("\n" if _Csr.IncBlankBeforeEndCsr else "") \
				+ _Csr.IncCsrEnd1Txt \
				+ _Csr.IncCsrEnd2Fmt.format(self.getItem('DeviceName'))
		self.needBlankLine = _Csr.IncBlankAfterEndCsr
		return text


	# Generate include file endGroup statement text.
	def generateIncludeFile_EndGroup(self, record):
		text	= ("\n" if _Csr.IncBlankBeforeEndGroup else "") \
				+ _Csr.IncLineIndentTxt \
				+ _Csr.IncGroupEndTxt
		self.needBlankLine = _Csr.IncBlankAfterEndGroup
		return text


	# Generate include file byte padding field text.
	def generateIncludeFile_ReserveBits(self, record):
		byteOffset	= self.byteOffset
		bitOffset	= self.bitsOffset
		data		= record['data']
		size		= data['Size']
		# Do nothing if no padding needed
		if 0 == size :
			return ""
		elif 0 > size :
			raise CsrCompilerError(self.errorLocation(),
									'Internal error (negative bit padding size): ' + str(size) + ' ' + str(record))
		#
		text	= ("\n" if _Csr.IncBlankBeforeReserveBits else "") \
				+ self.bitPadFmt.format(self.formatTypeSize(self.bitsDataType, self.bitsDataSize),
										_Csr.IncResvBitNameFmt.format(byteOffset, bitOffset, size),
										self.normalizeByteOffset(byteOffset, bitOffset),
										self.normalizeBitOffset(bitOffset))
		self.bitsOffset += size
		self.needBlankLine = _Csr.IncBlankAfterReserveBits
		return text


	# Generate include file byte padding field text.
	def generateIncludeFile_ReserveBytes(self, record):
		byteOffset	= self.byteOffset
		data		= record['data']
		size		= data['Size']
		# Do nothing if no padding needed
		if 0 == size :
			return ""
		elif 0 > size :
			raise CsrCompilerError(self.errorLocation(),
									'Internal error (negative byte padding size): ' + str(size) + ' ' + str(record))
		#
		text	= ("\n" if _Csr.IncBlankBeforeReserveBytes else "") \
				+ self.bytePadFmt.format(_Csr.IncDataTypeCharTxt,
											_Csr.IncResvByteNameFmt.format(byteOffset, size),
											byteOffset)
		self.byteOffset += size
		self.needBlankLine = _Csr.IncBlankAfterReserveBytes
		return text


	# Generate include file vebatim statement text.
	def generateIncludeFile_Verbatim(self, record):
		data	= record['data']
		text	= ("\n" if self.needBlankLine else "") \
				+ data['Text'] \
				+ "\n"
		self.needBlankLine = _Csr.IncBlankAfterVerbatim
		return text


	# Generate defines for bit fields.
	def generateIncludeFile_bitDefines(self):
		if 0 == len(self.bitDefines) :
			return ""
		text = _Csr.IncFileTlrBitsTxt
		converter = lambda text: int(text) if text.isdigit() else text
		sortKey = lambda key: [ converter(part) for part in re.split(r'(\d+)', key[0]) if part ]
		for name, dataSize, bitOffset, bitSize, invert in sorted(self.bitDefines, key = sortKey) :
			frmt = "0x{:0>" + str(_Csr.NibblesPerByte * dataSize) + "X}"
			invt = _Csr.IncBitInvertYes if invert else _Csr.IncBitInvertNo
			mask0 = (1 << bitSize) - 1
			mask1 = mask0 << bitOffset
			true0 = 0 if invert else mask0
			true1 = 0 if invert else mask1
			text	+=	"\n" \
					+	self.bitDefineFmt.format(name + _Csr.IncBitBitsSuffix, bitSize) \
					+	self.bitDefineFmt.format(name + _Csr.IncBitInvertSuffix, invt) \
					+	self.bitDefineFmt.format(name + _Csr.IncBitMask1Suffix, frmt.format(mask1)) \
					+	self.bitDefineFmt.format(name + _Csr.IncBitMask0Suffix, frmt.format(mask0)) \
					+	self.bitDefineFmt.format(name + _Csr.IncBitShiftSuffix, bitOffset) \
					+	self.bitDefineFmt.format(name + _Csr.IncBitSizeSuffix, dataSize) \
					+	self.bitDefineFmt.format(name + _Csr.IncBitTrue1Suffix, frmt.format(true1)) \
					+	self.bitDefineFmt.format(name + _Csr.IncBitTrue0Suffix, frmt.format(true0))
		self.bitDefines = []
		return text


	# Generate include file file header text.
	def generateIncludeFile_fileHeader(self):
		guard	= _Csr.IncGuardNameFmt.format(self.getItem('DeviceName').upper())
		detail	= self.getDetail()
		text	=	_Csr.IncFileHdrPart1Fmt.format(guard,
													guard,
													self.getItem('IncludePath'),
													self.getItem('CsrBrief'))
		if detail :
			text	+=	"\n" \
					+	self.formatText(detail,
										prefix = "",
										other = r'\n')
		text	+=	_Csr.IncFileHdrPart2Fmt.format(self.formatVersion(self.getItem('CompilerVersion')),
													self.getItem('CompileTime'),
													self.getItem('SourceFile'),
													self.getItem('SourceModified'),
													self.getItem('DeviceName'),
													str(self.getItem('DeviceType')),
													self.formatVersion(self.getItem('DeviceVersion'))) \
				+	_Csr.IncFileHdrPart3Txt
		return text


	# Generate the include file - Initialize the formats.
	def generateIncludeFile_initialize(self):
		self.byteOffset		= 0
		self.bitFirstTxt	= ""
		self.bitLastTxt		= ""
		self.bitDefines		= []
		#
		extraIndentLen		= _Csr.IncOffsetLen					+ len(_Csr.IncCommentBitDelimTxt) \
							+ _Csr.IncBitOffsetLen				+ len(_Csr.IncAccessDelimTxt) \
							+ _Csr.IncAccessLen					+ _Csr.IncAccessDelimLen
		# Maximum sizes of name areas.
		bitDataNameLen		= self.nameLengthBitData				+ len(_Csr.IncNameSuffixTxt)
		bitResvNameLen		= len(_Csr.IncResvBitPrefixTxt)			+ _Csr.IncOffsetLen \
							+ len(_Csr.IncResvBitDelimTxt)			+ _Csr.IncBitSizeLen \
							+ len(_Csr.IncBitSizePrefixTxt)			+ _Csr.IncBitSizeLen \
							+ len(_Csr.IncNameSuffixTxt)
		bitFieldNameLen		= self.nameLengthBitField				+ len(_Csr.IncBitSizePrefixTxt) \
							+ _Csr.IncBitSizeLen					+ len(_Csr.IncNameSuffixTxt)
		bitFieldNameLen		= max(bitFieldNameLen, bitResvNameLen)
		bitDefineNameLen	= self.nameLengthBitField				+ _Csr.IncMaxBitNameSuffixLen \
							+ _Csr.IncDefineDelimLen
		dataFieldNameLen	= self.nameLengthDataField				+ len(_Csr.IncNameSuffixTxt)
		# Maximum statement sizes (excluding comments).
		maxBitDataStmtLen	= 2 * _Csr.IncLineIndentLen				+ _Csr.IncDataTypeLen \
							+ bitDataNameLen
		maxBitFieldStmtLen	= 3 * _Csr.IncLineIndentLen				+ _Csr.IncDataTypeLen \
							+ bitFieldNameLen
		maxDefineStmtLen	= len(_Csr.IncDefineTxt)				+ self.nameLengthDefine \
							+ _Csr.IncDefineDelimLen				+ self.textLengthDefine
		maxResvBitStmtLen	= 3 * _Csr.IncLineIndentLen 			+ _Csr.IncDataTypeLen \
							+ len(_Csr.IncResvBitPrefixTxt)			+ _Csr.IncOffsetLen \
							+ len(_Csr.IncResvBitDelimTxt)			+ _Csr.IncBitSizeLen \
							+ len(_Csr.IncBitSizePrefixTxt)			+ _Csr.IncBitSizeLen
		maxResvByteStmtLen	= _Csr.IncLineIndentLen					+ _Csr.IncDataTypeLen \
							+ len(_Csr.IncResvBytePrefixTxt)		+ _Csr.IncOffsetLen \
							+ len(_Csr.IncResvByteSizePrefixTxt)	+ _Csr.IncOffsetLen \
							+ len(_Csr.IncResvByteSizeSuffixTxt)
		maxDataFieldStmtLen	= _Csr.IncLineIndentLen					+ _Csr.IncDataTypeLen \
							+ dataFieldNameLen
		maxDataFieldStmtLen	= max(maxDataFieldStmtLen, maxResvBitStmtLen, maxResvByteStmtLen)
		# Indent sizes.
		briefIndent			= _Csr.IncCommentDelimLen \
							+ max(maxBitDataStmtLen, maxBitFieldStmtLen, maxDefineStmtLen, maxDataFieldStmtLen)
		briefIndent			= max(briefIndent, _Csr.IncMinCommentColumn)
		detailIndent		= briefIndent + extraIndentLen
		otherIndent			= detailIndent + len(_Csr.IncCommentPrefixTxt)
		# Name formatting strings.
		dataNameFmt			= "{:<" + str(dataFieldNameLen) + "}"
		bitDataNameFmt		= "{:<" + str(bitDataNameLen) + "}"
		bitFieldNameFmt		= "{:<" + str(bitFieldNameLen) + "}"
		bitDefineNameFmt	= "{:<" + str(bitDefineNameLen) + "}"
		# Comment prefix padding sizes.
		dataFieldPadLen		= briefIndent - maxDataFieldStmtLen
		bitDataPadLen		= briefIndent - maxBitDataStmtLen
		bitFieldPadLen		= briefIndent - maxBitFieldStmtLen
		definePadLen		= briefIndent - maxDefineStmtLen
		#
		self.detailPrefix	= (' ' * detailIndent)				+ _Csr.IncCommentPrefixTxt
		self.detailOther	= "\n"								+ (' ' * otherIndent)
		self.detailSufix	=_Csr.IncCommentSuffixEolTxt
		#
		self.defineFmt		= _Csr.IncDefineTxt \
							+ "{:<" + str(self.nameLengthDefine + _Csr.IncDefineDelimLen) + "}" \
							+ "{:<" + str(self.textLengthDefine + definePadLen + extraIndentLen) + "}" \
							+ _Csr.IncCommentPrefixTxt \
							+ "{}" \
							+ _Csr.IncCommentSuffixEolTxt
		self.dataFieldFmt	= _Csr.IncLineIndentTxt				+ _Csr.IncDataTypeFmt \
							+ dataNameFmt						+ (' ' * dataFieldPadLen) \
							+ _Csr.IncCommentPrefixTxt			+ _Csr.IncOffsetFmt \
							+ _Csr.IncCommentNoBitTxt			+ _Csr.IncAccessDelimTxt \
							+ _Csr.IncAccessFmt					+ "{}" \
							+ _Csr.IncCommentSuffixEolTxt
		self.bytePadFmt		= _Csr.IncLineIndentTxt				+ _Csr.IncDataTypeFmt \
							+ dataNameFmt						+ (' ' * dataFieldPadLen) \
							+ _Csr.IncCommentPrefixTxt			+ _Csr.IncOffsetFmt \
							+ _Csr.IncCommentSuffixEolTxt
		self.bitPadFmt		= _Csr.IncLineIndentTxt				+ _Csr.IncLineIndentTxt \
							+ _Csr.IncLineIndentTxt				+ _Csr.IncDataTypeFmt \
							+ bitFieldNameFmt					+ (' ' * bitFieldPadLen) \
							+ _Csr.IncCommentPrefixTxt			+ _Csr.IncOffsetFmt \
							+ _Csr.IncCommentBitDelimTxt		+ _Csr.IncBitOffsetFmt \
							+ _Csr.IncCommentSuffixEolTxt
		self.bitBeginTxt	= _Csr.IncLineIndentTxt				+ _Csr.IncUnionStartTxt
		self.bitDataFmt		= _Csr.IncLineIndentTxt				+ _Csr.IncLineIndentTxt \
							+ _Csr.IncDataTypeFmt				+ bitDataNameFmt \
							+ (' ' * bitDataPadLen)				+ _Csr.IncCommentPrefixTxt \
							+ _Csr.IncOffsetFmt					+ _Csr.IncCommentBitDelimTxt \
							+ _Csr.IncBitOffsetNoneTxt			+ _Csr.IncAccessDelimTxt \
							+ _Csr.IncAccessFmt					+ "{}" \
							+ _Csr.IncCommentSuffixEolTxt
		self.bitFieldFmt	= _Csr.IncLineIndentTxt				+ _Csr.IncLineIndentTxt \
							+ _Csr.IncLineIndentTxt				+ _Csr.IncDataTypeFmt \
							+ bitFieldNameFmt					+ (' ' * bitFieldPadLen) \
							+ _Csr.IncCommentPrefixTxt			+ _Csr.IncOffsetFmt \
							+ _Csr.IncCommentBitDelimTxt		+ _Csr.IncBitOffsetFmt \
							+ _Csr.IncAccessDelimTxt			+ _Csr.IncAccessFmt \
							+ "{}"								+ _Csr.IncCommentSuffixEolTxt
		self.bitDefineFmt	= _Csr.IncDefineTxt					+ bitDefineNameFmt \
							+ "({})\n"
		self.bitFirstFmt	= _Csr.IncLineIndentTxt				+ _Csr.IncLineIndentTxt \
							+ _Csr.IncStructStartTxt
		self.bitLastFmt		= _Csr.IncLineIndentTxt				+ _Csr.IncLineIndentTxt \
							+ _Csr.IncStructEndTxt
		# Debugging.
		#print("nameLengthBitData     {}".format(self.nameLengthBitData))
		#print("nameLengthBitField    {}".format(self.nameLengthBitField))
		#print("nameLengthDataField   {}".format(self.nameLengthDataField))
		#print("nameLengthDefine      {}".format(self.nameLengthDefine))
		#print("textLengthDefine      {}".format(self.textLengthDefine))
		#print("bitDataNameLen        {}".format(bitDataNameLen))
		#print("bitFieldNameLen       {}".format(bitFieldNameLen))
		#print("bitDefineNameLen      {}".format(bitDefineNameLen))
		#print("dataFieldNameLen      {}".format(dataFieldNameLen))
		#print("maxBitDataStmtLen     {}".format(maxBitDataStmtLen))
		#print("maxBitFieldStmtLen    {}".format(maxBitFieldStmtLen))
		#print("maxDefineStmtLen      {}".format(maxDefineStmtLen))
		#print("maxDataFieldStmtLen   {}".format(maxDataFieldStmtLen))
		#print("briefIndent           {}".format(briefIndent))
		#print("detailIndent          {}".format(detailIndent))
		#print("otherIndent           {}".format(otherIndent))
		#print("dataFieldPadLen       {}".format(dataFieldPadLen))
		#print("bitDataPadLen         {}".format(bitDataPadLen))
		#print("bitFieldPadLen        {}".format(bitFieldPadLen))
		#print("definePadLen          {}".format(definePadLen))
		#print("dataNameFmt           '{}'".format(dataNameFmt))
		#print("bitDataNameFmt        '{}'".format(bitDataNameFmt))
		#print("bitFieldNameFmt       '{}'".format(bitFieldNameFmt))
		#print("defineFmt             '{}'".format(self.defineFmt))
		#print("dataFieldFmt          '{}'".format(self.dataFieldFmt))
		#print("bytePadFmt            '{}'".format(self.bytePadFmt))
		#print("bitPadFmt             '{}'".format(self.bitPadFmt))
		#print("self.bitDataFmt       '{}'".format(self.bitDataFmt))
		#print("self.bitFieldFmt      '{}'".format(self.bitFieldFmt))
		#print("self.bitDefineFmt     '{}'".format(self.bitDefineFmt))


	# Generate include file file header text.
	def generateIncludeFile_fileTrailer(self):
		text = _Csr.IncFileTlrTxt
		return text


	# Get a CSR item.
	def getItem(self, name):
		return self.csrData[name]


	# Get a CSR field.
	def getDetail(self):
		return self.csrData['CsrDetail'] if 'CsrDetail' in self.csrData else None


	# Initialize for a new CSR file.
	def initialize(self, args):
		self.byteOffset	= 0
		self.csrAccess	= '.' * (_Csr.BitsPerByte * _Csr.CsrSize)
		#
		self.isInBits	= False
		self.isInCsr	= False
		self.isInFile	= False
		self.isInGroup	= False
		self.sourceSubr	= 0
		#
		self.bitsInit()
		self.nameLengthBitData		= 0
		self.nameLengthBitField		= 0
		self.nameLengthDataField	= _Csr.IncMinDataNameLen
		self.nameLengthDefine		= _Csr.IncMinDefineNameLen
		self.textLengthDefine		= _Csr.IncMinDefineTextLen
		#
		self.defineNames	= []
		self.dataNames		= []
		#
		self.optionsParse(args)


	# Initialize the CSR information.
	def initializeCsrData(self, name, type, version, path, brief, detail):
		self.csrData = {}
		self.csrData['CompileTime']		= datetime.datetime.utcnow() \
										.isoformat(sep = ' ', timespec = 'seconds')
		self.csrData['CompilerVersion']	= self.compilerVersion()
		self.csrData['SourceFile']		= os.path.relpath(self.sourceFile)
		self.csrData['SourceModified']	= datetime.datetime.fromtimestamp(os.path.getmtime(self.sourceFile)) \
										.isoformat(sep = ' ', timespec = 'seconds')
		self.csrData['IncludePath']		= path
		self.csrData['self.csrData']			= []
		#
		self.csrData['DeviceName']		= name
		self.csrData['DeviceType']		= type
		self.csrData['DeviceVersion']	= {	'Major'		: version[0],
											'Minor'		: version[1],
											'Variant'	: version[2]}
		self.csrData['CsrBrief']		= brief
		if detail :
			self.csrData['CsrDetail']	= detail


	# Make an output file path/name.
	def makeFilePath(self, fileExtension, optionFilePath):
		if optionFilePath and 0 != len(optionFilePath) :
			# Break the file specific option into its parts
			path, tail	= os.path.split(optionFilePath)
			name, ext	= os.path.splitext(tail)
			#
			if 0 != len(path) and 0 != len(name) and 0 != len(ext) :
				return os.path.join(path, name) + ext
			if 0 != len(path) and 0 != len(name) and 0 == len(ext) :
				return os.path.join(path, name) + fileExtension
			if 0 != len(path) and 0 == len(name) and 0 != len(ext) :
				return os.path.join(path, self.options.outFile) + ext
			if 0 != len(path) and 0 == len(name) and 0 == len(ext) :
				return os.path.join(path, self.options.outFile) + fileExtension
			if 0 == len(path) and 0 != len(name) and 0 != len(ext) :
				return os.path.join(self.options.outDir, name) + ext
			if 0 == len(path) and 0 != len(name) and 0 == len(ext) :
				return os.path.join(self.options.outDir, name) + fileExtension
			if 0 == len(path) and 0 == len(name) and 0 != len(ext) :
				return os.path.join(self.options.outDir, self.options.outFile) + ext
		# Default
		return os.path.join(self.options.outDir, self.options.outFile) + fileExtension


	# Set the maximum field name length.
	def nameLengthDataFieldSet(self, name):
		self.dataNames = self.checkForDuplicate(self.dataNames, name)
		length = len(name)
		if self.nameLengthDataField < length :
			self.nameLengthDataField = length


	# Set the maximum define name length.
	def nameLengthDefineSet(self, name):
		self.defineNames = self.checkForDuplicate(self.defineNames, name)
		length = len(name)
		if self.nameLengthDefine < length :
			self.nameLengthDefine = length


	# Get normalized bit offset.
	def normalizeBitOffset(self, bitOffset):
		return int(bitOffset % _Csr.BitsPerByte)


	# Get normalized byte offset.
	def normalizeByteOffset(self, byteOffset, bitOffset):
		return byteOffset + int(bitOffset / _Csr.BitsPerByte)


	# Parse and extract command line options.
	def optionsParse(self, args):
		parser = _Csr._CsrArgumentParser(fromfile_prefix_chars = '@',
									description = 'CSR Compiler')
		parser.add_argument('--outdir',
							dest = 'outDir',
							help = 'Set default destination directory',
							metavar = 'outDir',
							default = os.getcwd())
		parser.add_argument('--outfile',
							dest = 'outFile',
							help = 'Set default destination file name',
							metavar = 'outFile',
							default = os.path.splitext(os.path.split(self.sourceFile)[1])[0])
		parser.add_argument('--include',
							dest = 'incDst',
							help = 'Set path/file/extension of the generated include file',
							metavar = 'incDst')
		parser.add_argument('--control',
							dest = 'ctlDst',
							help = 'Set path/file/extension of the generated control file',
							metavar = 'ctlDst')
		parser.add_argument('--listing',
							dest = 'lstDst',
							help = 'Set path/file/extension of the generated listing file',
							metavar = 'lstDst')
		parser.add_argument('--version',
							action = 'store_true',
							dest = 'showVersion',
							help = 'Show version information')
		parser.add_argument('-v', '--verbose',
							action = 'count',
							dest = 'verbosity',
							default = 0,
							help = 'Increase output verbosity (show: devices, file generation, input statments, include statements)')
		self.options = parser.parse_args(args)
		if self.options.showVersion :
			self.showVersion()
			exit()


	# Ensure that we are within a beginBits/endBits block.
	def requireInBits(self):
		if not self.isInBits :
			raise CsrCompilerError(self.errorLocation(), 'Missing beginBits statement')


	# Ensure that we are within a beginCsr/endCsr block.
	def requireInCsr(self):
		if not self.isInCsr :
			raise CsrCompilerError(self.errorLocation(), 'Missing beginCsr statement')


	# Ensure that we are within a beginFile/endFile block.
	def requireInFile(self):
		if not self.isInFile :
			raise CsrCompilerError(self.errorLocation(), 'Missing beginFile statement')


	# Ensure that we are within a beginGroup/endGroup block.
	def requireInGroup(self):
		if not self.isInGroup :
			raise CsrCompilerError(self.errorLocation(), 'Missing beginGroup statement')


	# Ensure that we are not within a beginBits/endBits block.
	def requireNotInBits(self):
		if self.isInBits :
			raise CsrCompilerError(self.errorLocation(), 'Not allowed within a beginBits/endBits block')


	# Ensure that we are not within a beginCsr/endCsr block.
	def requireNotInCsr(self):
		if self.isInCsr :
			raise CsrCompilerError(self.errorLocation(), 'Not allowed within a beginCsr/endCsr block')


	# Ensure that we are within a beginFile/endFile block.
	def requireNotInFile(self):
		if self.isInFile :
			raise CsrCompilerError(self.errorLocation(), 'Duplicate beginFile statement')


	# Ensure that we are not within a beginGroup/endGroup block.
	def requireNotInGroup(self):
		if self.isInGroup :
			raise CsrCompilerError(self.errorLocation(), 'Not allowed within a beginGroup/endGroup block')


	# Show the CSR compiler version.
	def showVersion(self):
		print("CSR Compiler {}".format(self.formatVersion(self.compilerVersion())))


	# Set the source file name from the given stack frame.
	def sourceFileSet(self, frame):
		self.sourceFile = inspect.getframeinfo(frame[0]).filename


	# Get the source line number.
	def sourceLineGet(self):
		return self.sourceSubr if self.sourceSubr else self.sourceLine


	# Set the source line number from the given stack frame.
	def sourceLineSet(self, frame):
		self.sourceLine = inspect.getframeinfo(frame[0]).lineno


	# Set the current statement.
	def statementSet(self, type, args = ''):
		#
		maximumErrorArgumentLength = 32
		#
		if self.sourceSubr :
			self.sourceSubr -= 1
		self.statementArgs	= args
		self.statementError	= type
		self.statementType	= type
		self.sourceLineSet(inspect.stack()[2])
		# Create the error statement
		if args :
			suffix = ''
			end = args.find('\n')
			if 0 < end :
				suffix	= '...'
				end		= max(maximumErrorArgumentLength, end)
			else:
				end = len(args)
				if maximumErrorArgumentLength < end :
					suffix = '...'
					end = maximumErrorArgumentLength
			self.statementError += " " + args[: end] + suffix


	# Set the maximum define text length.
	def textLengthDefineSet(self, name):
		length = len(name)
		if self.textLengthDefine < length :
			self.textLengthDefine = length


	# Get the size in byte of a data type.
	def typeSize(self, type):
		return int(int(type[1:]) / _Csr.BitsPerByte)


	# Check verbosity level - Show device names.
	def verbosityShowDeviceNames(self):
		return self.options.verbosity >= 1


	# Check verbosity level - Show device names.
	def verbosityShowIncludeStatements(self):
		return self.options.verbosity >= 4


	# Check verbosity level - Show device names.
	def verbosityShowInputStatements(self):
		return self.options.verbosity >= 3


	# Check verbosity level - Show device names.
	def verbosityShowOutputFiles(self):
		return self.options.verbosity >= 2

###############################################################################

_csr = _Csr()

###############################################################################

# Process a beginBits statement - Begin a block of bits.
def beginBits(name = None, type = None, access = '?', brief = None, detail = None):
	global _csr
	# Statement tracing.
	form	= "beginBits"
	name	= _csr.cleanName(name)
	type	= _csr.cleanDataType(type)
	access	= _csr.cleanAccess(access)
	brief	= _csr.cleanSingleLineText(brief)
	detail	= _csr.cleanMultiLineText(detail)
	args	= "name={}, type={}, access={}".format(name, type, access) \
			+ _csr.formatTextArg(brief, caption = 'brief=') \
			+ _csr.formatTextArg(detail, caption = 'detail=')
	_csr.statementSet(form, args)
	_csr.fileListRecordStatement(form, args)
	# Check for errors.
	_csr.requireInCsr()
	_csr.requireNotInBits()
	_csr.checkName(name)
	dataType = _csr.checkDataType(type)
	_csr.checkAccess(access)
	_csr.checkSingleLineText(brief, 'brief')
	_csr.checkMultiLineText(detail)
	#
	_csr.bitDataNameLengthSet(name)
	#
	_csr.bitsAccess		= _Csr.Access_ToInternal[access]
	_csr.bitsOffset		= 0
	_csr.bitsDataSize	= _csr.typeSize(dataType)
	_csr.bitsDataType	= dataType[0]
	_csr.bitsBitSize	= _csr.bitsDataSize * _Csr.BitsPerByte
	data = {'Name'		: name,
			'Type'		: _csr.bitsDataType,
			'Size'		: _csr.bitsDataSize,
			'Access'	: _csr.bitsAccess,
			'Brief'		: brief}
	if detail :
		data['Detail'] = detail
	_csr.addData('beginBits', data)
	_csr.isInBits = True


# Process a beginCsr statement - Begin the CSR structure declaration.
def beginCsr():
	global _csr
	# Statement tracing.
	form = "beginCsr"
	_csr.statementSet(form)
	_csr.fileListRecordStatement(form)
	# Check for errors.
	_csr.requireInFile()
	_csr.requireNotInCsr()
	#
	_csr.addData('beginCsr')
	_csr.isInCsr = True


# Process a beginFile statement - Begin the CSR compilation and the generation of the files.
def beginFile(name = None, type = None, version = None, argv = None, brief = None, detail = None, path = None):
	global _csr
	# Get the source file information.
	frame = inspect.stack()[1]
	_csr.sourceFileSet(frame)
	_csr.sourceLineSet(frame)
	del frame
	# Initialize and get the command line options.
	_csr.initialize(argv)
	# Create the listing file.
	_csr.fileLstOpen()
	# Statement tracing.
	form	= "beginFile"
	name	= _csr.cleanName(name)
	brief	= _csr.cleanSingleLineText(brief)
	detail	= _csr.cleanMultiLineText(detail)
	if path :
		path	= path.strip()
	else:
		path	= name.lower()
	args	=	"name={}, type={}, version={}".format(name, type, version) \
			+ _csr.formatTextArg(path, caption = 'path=') \
			+ _csr.formatTextArg(brief, caption = 'brief=') \
			+ _csr.formatTextArg(detail, caption = 'detail=')
	if argv :
		args += _csr.formatTextArg(' '.join(argv), caption = 'argv=')
	_csr.statementSet(form, args)
	if _csr.verbosityShowDeviceNames() :
		print("{}Processing device {}:".format(_csr.newDevDelim, str(name)))
		_csr.newDevDelim = '\n'
	_csr.fileListRecordStatement(form, args)
	# Check for errors.
	_csr.requireNotInFile()
	_csr.checkName(name)
	_csr.checkDeviceType(type)
	_csr.checkSingleLineText(brief, 'brief')
	_csr.checkDeviceVersion(version)
	_csr.checkMultiLineText(detail)
	#
	_csr.initializeCsrData(name, type, version, path, brief, detail)
	_csr.isInFile = True


# Process a beginGroup statement - Begin a data group.
def beginGroup(name = None, detail = None):
	global _csr
	# Statement tracing.
	form	= "beginGroup"
	name	= _csr.cleanSingleLineText(name)
	detail	= _csr.cleanMultiLineText(detail)
	args	= 'name="{}"'.format(name) \
			+ _csr.formatTextArg(detail, caption = 'detail=')
	_csr.statementSet(form, args)
	_csr.fileListRecordStatement(form, args)
	# Check for errors.
	_csr.requireInCsr()
	_csr.requireNotInGroup()
	_csr.checkSingleLineText(name, 'name')
	_csr.checkMultiLineText(detail)
	#
	data = {'Name' : name}
	if detail :
		data['Detail'] = detail
	_csr.addData('beginGroup', data)
	_csr.isInGroup = True


# Process a bitField statement - Declare a CSR structure bit field.
def bitField(name = None, size = None, access = '?', brief = None, detail = None, invert = False):
	global _csr
	# Statement tracing.
	form	= "bitField"
	name	= _csr.cleanName(name)
	access	= _csr.cleanAccess(access)
	invert	= _csr.cleanBoolean(invert)
	brief	= _csr.cleanSingleLineText(brief)
	detail	= _csr.cleanMultiLineText(detail)
	args	=	"name={}, size={}, access={}, invert={}".format(name, size, access, invert) \
			+ _csr.formatTextArg(brief, caption = 'brief=') \
			+ _csr.formatTextArg(detail, caption = 'detail=')
	_csr.statementSet(form, args)
	_csr.fileListRecordStatement(form, args)
	# Check for errors.
	_csr.requireInBits()
	_csr.checkName(name)
	_csr.checkBitSize(size)
	_csr.checkAccess(access)
	_csr.checkSingleLineText(brief, 'brief')
	_csr.checkMultiLineText(detail)
	#
	_csr.bitFieldNameLengthSet(name)
	#
	access = _Csr.Access_ToInternal[access]
	data = {'Name'		: name,
			'Size'		: size,
			'Access'	: access,
			'Invert'	: invert,
			'Brief'		: brief}
	if detail :
		data['Detail'] = detail
	_csr.addData('bitField', data)
	# Mark the bits as being used.
	_csr.accessSetNextBits(size, access)
	_csr.bitsOffset += size


# Process a comment statement - Add a comment to the generated files.
def comment(text = None):
	global _csr
	# Statement tracing.
	form	= "comment"
	text	= _csr.cleanMultiLineText(text)
	if text :
		args	= _csr.formatText(text,
									delimiter = '"',
									other = r'\n' + (' ' * (1 + _Csr.LstArgumentIndentSize)))
	else:
		args	= ""
	_csr.statementSet(form, args)
	_csr.fileListRecordStatement(form, args)
	# Check for errors.
	_csr.requireInFile()
	_csr.checkMultiLineText(text)
	#
	if text :
		data = {'Text' : text}
	else:
		data = None
	_csr.addData('comment', data)


# Process a dataField statement - Declare a CSR structure data field.
def dataField(name = None, type = None, access = '?', brief = None, detail = None):
	global _csr
	# Statement tracing.
	form	= "dataField"
	name	= _csr.cleanName(name)
	type	= _csr.cleanDataType(type)
	access	= _csr.cleanAccess(access)
	brief	= _csr.cleanSingleLineText(brief)
	detail	= _csr.cleanMultiLineText(detail)
	args =	"name={}, type={}, access={}".format(name, type, access) \
			+ _csr.formatTextArg(brief, caption = 'brief=') \
			+ _csr.formatTextArg(detail, caption = 'detail=')
	_csr.statementSet(form, args)
	_csr.fileListRecordStatement(form, args)
	# Check for errors.
	_csr.requireInCsr()
	_csr.requireNotInBits()
	_csr.checkName(name)
	dataType = _csr.checkDataType(type)
	_csr.checkAccess(access)
	_csr.checkSingleLineText(brief, 'brief')
	_csr.checkMultiLineText(detail)
	size	= _csr.typeSize(dataType)
	if _csr.byteOffset + size - 1 >= _Csr.CsrSize :
		raise CsrCompilerError(_csr.errorLocation(), 'CSR overflow')
	#
	_csr.nameLengthDataFieldSet(name)
	#
	access = _Csr.Access_ToInternal[access]
	data = {'Name'		: name,
			'Type'		: dataType[0],
			'Size'		: size,
			'Access'	: access,
			'Brief'		: brief}
	if detail :
		data['Detail'] = detail
	_csr.addData('dataField', data)
	# Mark the bytes as being used.
	_csr.accessSetNextBytes(size, access)
	_csr.byteOffset += size


# Process a define statment - Add a C/C++ preprocessor #define to the generated files.
def define(name = None, text = None, brief = None, detail = None):
	global _csr
	# Statement tracing.
	form	= "define"
	name	= _csr.cleanName(name)
	text	= _csr.cleanDefineText(text)
	brief	= _csr.cleanSingleLineText(brief)
	detail	= _csr.cleanMultiLineText(detail)
	args	= "name={}, text={}".format(name, text) \
			+ _csr.formatTextArg(brief, caption = 'brief=') \
			+ _csr.formatTextArg(detail, caption = 'detail=')
	_csr.statementSet(form, args)
	_csr.fileListRecordStatement(form, args)
	# Check for errors.
	_csr.requireInFile()
	_csr.checkName(name)
	_csr.checkDefineText(text)
	_csr.checkSingleLineText(brief, 'brief')
	_csr.checkMultiLineText(detail)
	#
	_csr.nameLengthDefineSet(name)
	_csr.textLengthDefineSet(text)
	#
	data = {'Name'	: name,
			'Text'	: text,
			'Brief'	: brief}
	if detail :
		data['Detail'] = detail
	_csr.addData('define', data)
	

# Process a endBits statement - End a block of bits.
def endBits():
	global _csr
	# Statement tracing.
	form = "endBits"
	_csr.statementSet(form)
	_csr.fileListRecordStatement(form)
	# Check for errors.
	_csr.requireInBits()
	# How many bits are remaining?
	size = _csr.bitsBitSize - _csr.bitsOffset
	#
	data = {'Size'	: size}
	_csr.addData('endBits', data)
	_csr.byteOffset += _csr.bitsDataSize
	#
	_csr.bitsInit()
	_csr.isInBits = False


# Process an endCsr statement - End the CSR structure declaration.
def endCsr():
	global _csr
	# Statement tracing.
	form = "endCsr"
	_csr.statementSet(form)
	_csr.fileListRecordStatement(form)
	# Check for errors.
	_csr.requireInCsr()
	#
	_csr.addData('endCsr')
	_csr.isInCsr = False


# Process an endFile statement - End the CSR compilation and the generation of the files.
def endFile():
	global _csr
	# Statement tracing.
	form = "endFile"
	_csr.statementSet(form)
	_csr.fileListRecordStatement(form)
	# Check for errors.
	_csr.requireInFile()
	_csr.requireNotInGroup()
	_csr.requireNotInCsr()
	#
	_csr.isInFile = False
	_csr.generateControlFile()
	_csr.generateIncludeFile()
	_csr.fileListRecordCsrData()
	_csr.fileLstClose()


# Process an endGroup statement - End a data group.
def endGroup():
	global _csr
	# Statement tracing.
	form = "endGroup"
	_csr.statementSet(form)
	_csr.fileListRecordStatement(form)
	# Check for errors.
	_csr.requireInGroup()
	#
	_csr.addData('endGroup')
	_csr.isInGroup = False


# Process a reserve statement - Reserve bytes in the CSR.
def reserveBits(size):
	global _csr
	# Statement tracing.
	form	= "reserveBits"
	args	= str(size)
	_csr.statementSet(form, args)
	_csr.fileListRecordStatement(form, args)
	# Check for errors.
	_csr.requireInBits()
	_csr.checkReserveBits(size)
	#
	data = {'Size' : size}
	_csr.addData('reserveBits', data)
	_csr.bitsOffset += size


# Process a reserve statement - Reserve bytes in the CSR.
def reserveBytes(size):
	global _csr
	# Statement tracing.
	form	= "reserveBytes"
	args	= str(size)
	_csr.statementSet(form, args)
	_csr.fileListRecordStatement(form, args)
	# Check for errors.
	_csr.requireNotInBits()
	_csr.checkReserveBytes(size)
	#
	data = {'Size' : size}
	_csr.addData('reserveBytes', data)
	_csr.byteOffset += size


# Process a setBitOffset statement - Reserve bytes in the CSR.
def setBitOffset(offset):
	global _csr
	# Statement tracing.
	form	= "setBitOffset"
	args	= str(offset)
	_csr.statementSet(form, args)
	_csr.fileListRecordStatement(form, args)
	# Check for errors.
	_csr.requireInBits()
	_csr.checkBitOffset(offset)
	# Is any padding needed?
	size = offset - _csr.bitsOffset
	if size :
		data = {'Size' : size}
		_csr.addData('reserveBits', data)
		_csr.bitsOffset = offset


# Process a setByteOffset statement - Reserve bytes in the CSR.
def setByteOffset(offset):
	global _csr
	# Statement tracing.
	form	= "setByteOffset"
	args	= str(offset)
	_csr.statementSet(form, args)
	_csr.fileListRecordStatement(form, args)
	# Check for errors.
	_csr.requireNotInBits()
	_csr.checkByteOffset(offset)
	# Is any padding needed?
	size = offset - _csr.byteOffset
	if size :
		data = {'Size' : size}
		_csr.addData('reserveBytes', data)
		_csr.byteOffset = offset


# Define the standard CSR registers.
def standardCsrRegisters():
	global _csr
	# ToDo: Add additional detailed descriptions.
	_csr.sourceSubr = -1
	#
	setByteOffset(240)
	beginGroup('Standard VideoRay CSR protocol registers',
				detail="""
						These registers are common to all devices.

						Each of these registers should only be accessed
						(read from or written to) individually and not
						by general contiguous reads or writes.""")
	dataField('custom_command',			'U32',	'W',	'Custom command register',
				detail="""
						Data sent to this register is interpreted as a
						multi-byte command and not a CSR memory read/write.
						It is intended that this address be used for
						devices which wish to implement a different
						communications protocol.""")
	dataField('factory_service_data',	'U32',	'R',	'Device specific service data')
	dataField('config_data_size',		'U16',	'R',	'Configuration data size')
	dataField('config_data',			'U8',	'R',	'Configuration data register')
	dataField('firmware_version',		'U8',	'R',	'Firmware version')
	dataField('NODE_ID',				'U8',	'rw',	'Node ID')
	dataField('GROUP_ID',				'U8',	'R',	'Group ID')
	dataField('utility_command',		'U16',	'W',	'Utility command register',
				detail="""
						The utility command register is used for commands
						that are universsal to all devices.""")
	endGroup()
	define('UtlCmdReboot',		'(0xdead)',	'Reboot the device',
				detail="""This command causes the device to reboot.""")
	define('UtlCmdRequestId',	'(0x4449)',	'Send device ID packet',
				detail="""
						This command causes the device to transmit its
						ID packet. The response is sent using a
						carrier-sense multiple access with collision
						detection (CDMA/CD) protocol in order to
						maximize the chance of reception.""")
	define('UtlCmdSetNodeId',	'(0x4e53)',	'Set device node and group IDs',
				detail="""
						This command sets the device's node and group
						IDs via a broadcast (multicast) message. The
						payload must contain the NUL terminated
						serial number, the new node ID, and the new
						group ID.""")
	#
	_csr.sourceSubr = 0


# Process a verbatim statement - Output text unchanged to the generated include file.
def verbatim(text = None):
	global _csr
	# Statement tracing.
	form	= "verbatim"
	text	= _csr.cleanMultiLineText(text)
	args	= _csr.formatText(text,
								delimiter = '"',
								other = r'\n' + (' ' * (1 + _Csr.LstArgumentIndentSize)))
	_csr.statementSet(form, args)
	_csr.fileListRecordStatement(form, args)
	# Check for errors.
	if not text :
		raise CsrCompilerError(_csr.errorLocation(), 'Missing argument')
	elif 0 == len(text) :
		raise CsrCompilerError(_csr.errorLocation(), 'Empty argument')
	_csr.requireInFile()
	_csr.checkMultiLineText(text)
	#
	if text :
		data = {'Text' : text}
	else:
		data = None
	_csr.addData('verbatim', data)


########################################################################################################################

# Standard CSR file text
StandardCsrDescription = "The CSR memory space is " + str(_Csr.CsrSize) + """ bytes and presents the interface to the device's
							configion and status registers which are accessible through the VideoRay
							communication protocol. The entire CSR memory space is defined by this
							structure. It is assumed that the structure is packed and byte aligned."""

# Data type aliases
u8			= 'U8'
uint8		= 'U8'
uint8_t		= 'U8'
quint8		= 'U8'
u16			= 'U16'
uint16		= 'U16'
uint16_t	= 'U16'
quint16		= 'U16'
u32			= 'U32'
uint32		= 'U32'
uint32_t	= 'U32'
quint32		= 'U32'
u64			= 'U64'
uint64		= 'U64'
uint64_t	= 'U64'
quint64		= 'U64'

s8			= 'S8'
sint8		= 'S8'
int8_t		= 'S8'
qint8		= 'S8'
s16			= 'S16'
sint16		= 'S16'
int16_t		= 'S16'
qint16		= 'S16'
s32			= 'S32'
sint32		= 'S32'
int32_t		= 'S32'
qint32		= 'S32'
s64			= 'S64'
sint64		= 'S64'
int64_t		= 'S64'
qint64		= 'S64'

f32			= 'F32'
float		= 'F32'
f64			= 'F64'
double		= 'F64'
qreal		= 'F64'
