import csr
import sys


def test_1(testNumber, argv):
	csr.beginFile('Csr_Compiler_Test_' + str(testNumber), 254, (0,1,1), argv = argv, path = 'test_suite',
			brief	= 'Test #' + str(testNumber) + ' of CSR compiler',
			detail	= """
						This is a test of the CSR compiler and is not intended for actual use.
						
						""" + csr.StandardCsrDescription)
	csr.define(' CSR_TEST ', "({})".format(testNumber),
			brief	= ' Test Number ',
			detail	= """
						This value indicates the CSR test number.
						""")
	csr.beginCsr()
	#
	csr.beginGroup('Check data types')
	csr.dataField(name = 'u8',		type = 'u8',		brief = '1-byte unsigned integer')
	csr.dataField(name = 'u8_',		type = 'u8 ',		brief = '1-byte unsigned integer')
	csr.dataField(name = 'u_8',		type = 'u 8',		brief = '1-byte unsigned integer')
	csr.dataField(name = 'u_8_',	type = 'u 8 ',		brief = '1-byte unsigned integer')
	csr.dataField(name = '_u8',		type = ' u8',		brief = '1-byte unsigned integer')
	csr.dataField(name = '_u8_',	type = ' u8 ',		brief = '1-byte unsigned integer')
	csr.dataField(name = '_u_8',	type = ' u 8',		brief = '1-byte unsigned integer')
	csr.dataField(name = '_u_8_',	type = ' u 8 ',		brief = '1-byte unsigned integer')
	csr.dataField(name = 'U8',		type = 'U8',		brief = '1-byte unsigned integer')
	csr.dataField(name = 'U8_',		type = 'U8 ',		brief = '1-byte unsigned integer')
	csr.dataField(name = 'U_8',		type = 'U 8',		brief = '1-byte unsigned integer')
	csr.dataField(name = 'U_8_',	type = 'U 8 ',		brief = '1-byte unsigned integer')
	csr.dataField(name = '_U8',		type = ' U8',		brief = '1-byte unsigned integer')
	csr.dataField(name = '_U8_',	type = ' U8 ',		brief = '1-byte unsigned integer')
	csr.dataField(name = '_U_8',	type = ' U 8',		brief = '1-byte unsigned integer')
	csr.dataField(name = '_U_8_',	type = ' U 8 ',		brief = '1-byte unsigned integer')
	csr.dataField(name = 'u16',		type = 'u16',		brief = '2-byte unsigned integer')
	csr.dataField(name = 'u16_',	type = 'u16 ',		brief = '2-byte unsigned integer')
	csr.dataField(name = 'u1_6',	type = 'u1 6',		brief = '2-byte unsigned integer')
	csr.dataField(name = 'u1_6_',	type = 'u1 6 ',		brief = '2-byte unsigned integer')
	csr.dataField(name = 'u_16',	type = 'u 16',		brief = '2-byte unsigned integer')
	csr.dataField(name = 'u_16_',	type = 'u 16 ',		brief = '2-byte unsigned integer')
	csr.dataField(name = 'u_1_6',	type = 'u 1 6',		brief = '2-byte unsigned integer')
	csr.dataField(name = 'u_1_6_',	type = 'u 1 6 ',	brief = '2-byte unsigned integer')
	csr.dataField(name = '_u16',	type = ' u16',		brief = '2-byte unsigned integer')
	csr.dataField(name = '_u16_',	type = ' u16 ',		brief = '2-byte unsigned integer')
	csr.dataField(name = '_u1_6',	type = ' u1 6',		brief = '2-byte unsigned integer')
	csr.dataField(name = '_u1_6_',	type = ' u1 6 ',	brief = '2-byte unsigned integer')
	csr.dataField(name = '_u_16',	type = ' u 16',		brief = '2-byte unsigned integer')
	csr.dataField(name = '_u_16_',	type = ' u 16 ',	brief = '2-byte unsigned integer')
	csr.dataField(name = '_u_1_6',	type = ' u 1 6',	brief = '2-byte unsigned integer')
	csr.dataField(name = '_u_1_6_',	type = ' u 1 6 ',	brief = '2-byte unsigned integer')
	csr.dataField(name = 'U16',		type = 'U16',		brief = '2-byte unsigned integer')
	csr.dataField(name = 'u32',		type = 'u32',		brief = '4-byte unsigned integer')
	csr.dataField(name = 'U32',		type = 'U32',		brief = '4-byte unsigned integer')
	csr.dataField(name = 'u64',		type = 'u64',		brief = '8-byte unsigned integer')
	csr.dataField(name = 'U64',		type = 'U64',		brief = '8-byte unsigned integer')
	csr.dataField(name = 's8',		type = 's8',		brief = '1-byte signed integer')
	csr.dataField(name = 'S8',		type = 'S8',		brief = '1-byte signed integer')
	csr.dataField(name = 's16',		type = 's16',		brief = '2-byte signed integer')
	csr.dataField(name = 'S16',		type = 'S16',		brief = '2-byte signed integer')
	csr.dataField(name = 's32',		type = 's32',		brief = '4-byte signed integer')
	csr.dataField(name = 'S32',		type = 'S32',		brief = '4-byte signed integer')
	csr.dataField(name = 's64',		type = 's64',		brief = '8-byte signed integer')
	csr.dataField(name = 'S64',		type = 'S64',		brief = '8-byte signed integer')
	csr.dataField(name = 'f32',		type = 'f32',		brief = '4-byte floating point')
	csr.dataField(name = 'F32',		type = 'F32',		brief = '4-byte floating point')
	csr.dataField(name = 'f64',		type = 'f64',		brief = '8-byte floating point')
	csr.dataField(name = 'F64',		type = 'F64',		brief = '8-byte floating point')
	csr.endGroup()
	#
	csr.standardCsrRegisters()
	csr.endCsr()
	csr.endFile()


def test_2(testNumber, argv):
	csr.beginFile('Csr_Compiler_Test_' + str(testNumber), 254, (0,1,2), argv = argv, path = 'test_suite',
			brief	= 'Test #' + str(testNumber) + ' of CSR compiler',
			detail	= """
						This is a test of the CSR compiler and is not intended for actual use.
						
						""" + csr.StandardCsrDescription)
	csr.define(' CSR_TEST ', "({})".format(testNumber),
			brief	= ' Test Number ',
			detail	= """
						This value indicates the CSR test number.
						""")
	csr.beginCsr()
	#
	csr.beginGroup('Check access codes')
	csr.dataField(name = 'u8',		type = 'u8',	access = 'r',		brief = 'Access: r     - Read only')
	csr.dataField(name = 'u8_',		type = 'u8 ',	access = 'r ',		brief = 'Access: r_    - Read only')
	csr.dataField(name = 'u_8',		type = 'u 8',	access = ' r',		brief = 'Access: _r    - Read only')
	csr.dataField(name = 'u_8_',	type = 'u 8 ',	access = ' r ',		brief = 'Access: _r_   - Read only')
	csr.dataField(name = '_u8',		type = ' u8',	access = 'w',		brief = 'Access: w     - Write only')
	csr.dataField(name = '_u8_',	type = ' u8 ',	access = 'w ',		brief = 'Access: w_    - Write only')
	csr.dataField(name = '_u_8',	type = ' u 8',	access = ' w',		brief = 'Access: _w    - Write only')
	csr.dataField(name = '_u_8_',	type = ' u 8 ',	access = ' w ',		brief = 'Access: _w_   - Write only')
	csr.dataField(name = 'U8',		type = 'U8',	access = 'rw',		brief = 'Access: rw    - Read/write')
	csr.dataField(name = 'U8_',		type = 'U8 ',	access = 'rw ',		brief = 'Access: rw_   - Read/write')
	csr.dataField(name = 'U_8',		type = 'U 8',	access = ' rw',		brief = 'Access: _rw   - Read/write')
	csr.dataField(name = 'U_8_',	type = 'U 8 ',	access = ' rw ',	brief = 'Access: _rw_  - Read/write')
	csr.dataField(name = '_U8',		type = ' U8',	access = ' r w ',	brief = 'Access: _r_w_ - Read/write')
	csr.dataField(name = '_U8_',	type = ' U8 ',	access = 'wr',		brief = 'Access: wR    - Read/write')
	csr.dataField(name = '_U_8',	type = ' U 8',	access = 'R',		brief = 'Access: R     - Read only')
	csr.dataField(name = '_U_8_',	type = ' U 8 ',	access = 'W',		brief = 'Access: W     - Write only')
	csr.dataField(name = 'u16',		type = 'u16',	access = 'Rw',		brief = 'Access: Rw    - Read/write')
	csr.dataField(name = 'u16_',	type = 'u16 ',	access = 'Wr',		brief = 'Access: Wr    - Read/write')
	csr.dataField(name = 'u1_6',	type = 'u1 6',	access = 'rW',		brief = 'Access: rW    - Read/write')
	csr.dataField(name = 'u1_6_',	type = 'u1 6 ',	access = 'RW',		brief = 'Access: RW    - Read/write')
	csr.dataField(name = 'u_16',	type = 'u 16',	access = 'b',		brief = 'Access: b     - Read/write')
	csr.dataField(name = 'u_16_',	type = 'u 16 ',	access = 'B',		brief = 'Access: B     - Read/write')
	csr.dataField(name = 'u_1_6',	type = 'u 1 6',	access = '?',		brief = 'Access: ?     - Undefined')
	csr.dataField(name = 'u_1_6_',	type = 'u 1 6 ',					brief = 'Access: Not set - Undefined')
	csr.endGroup()
	#
	csr.standardCsrRegisters()
	csr.endCsr()
	csr.endFile()


def test_3(testNumber, argv):
	csr.beginFile('Csr_Compiler_Test_' + str(testNumber), 254, (0,1,2), argv = argv, path = 'test_suite',
			brief	= 'Test #' + str(testNumber) + ' of CSR compiler',
			detail	= """
						This is a test of the CSR compiler and is not intended for actual use.
						
						""" + csr.StandardCsrDescription)
	csr.define(' CSR_TEST ', "({})".format(testNumber),
			brief	= ' Test Number ',
			detail	= """
						This value indicates the CSR test number.
						""")
	csr.beginCsr()
	#
	csr.dataField('u16',	'u16',	'RW',	'2-byte unsigned integer')
	#
	csr.beginBits('u32',	'u32',	'RW',	'1-byte unsigned integer')
	csr.bitField('b0',	1,	'B',	'1-bit unsigned integer')
	csr.bitField('b1',	2,	'R',	'2-bit unsigned integer')
	csr.reserveBits(2)
	csr.bitField('b5',	1,	'R',	'1-bit unsigned integer', invert = True)
	csr.setBitOffset(12)
	csr.bitField('b16',	2,	'R',	'2-bit unsigned integer')
	csr.endBits()
	#
	csr.beginBits('U32',	'u32',	'RW',	'1-byte unsigned integer')
	csr.bitField('B0',	1,	'B',	'1-bit unsigned integer')
	csr.bitField('B1',	2,	'R',	'2-bit unsigned integer')
	csr.bitField('B3',	3,	'R',	'3-bit unsigned integer', invert = True)
	csr.bitField('B6',	4,	'R',	'4-bit unsigned integer', invert = True)
	csr.bitField('B10',	5,	'R',	'5-bit unsigned integer')
	csr.bitField('B15',	6,	'R',	'6-bit unsigned integer')
	csr.bitField('B21',	7,	'R',	'7-bit unsigned integer')
	csr.endBits()
	#
	csr.standardCsrRegisters()
	csr.endCsr()
	csr.endFile()

def test_4(testNumber, argv):
	csr.beginFile('Csr_Compiler_Test_' + str(testNumber), 254, (0,1,2), argv = argv, path = 'test_suite',
			brief	= 'Test #' + str(testNumber) + ' of CSR compiler',
			detail	= """
						This is a test of the CSR compiler and is not intended for actual use.
						
						""" + csr.StandardCsrDescription)
	csr.define(' CSR_TEST ', "({})".format(testNumber),
			brief	= ' Test Number ',
			detail	= """
						This value indicates the CSR test number.
						""")
	csr.beginCsr()
	#
	csr.comment()
	csr.comment('Single line C comment.')
	csr.comment()
	#
	csr.comment()
	#
	csr.dataField('u8',		'u8',	'RW',	'1-byte unsigned integer')
	csr.dataField('U8',		'U8',	'RW',	'1-byte unsigned integer')
	csr.dataField('u16',	'u16',	'RW',	'2-byte unsigned integer')
	csr.dataField('U16',	'U16',	'RW',	'2-byte unsigned integer')
	csr.dataField('u32',	'u32',	'RW',	'4-byte unsigned integer')
	csr.dataField('U32',	'U32',	'RW',	'4-byte unsigned integer')
	csr.dataField('u64',	'u64',	'RW',	'8-byte unsigned integer')
	csr.dataField('U64',	'U64',	'RW',	'8-byte unsigned integer')
	csr.dataField('s8',		's8',	'RW',	'1-byte signed integer')
	csr.dataField('S8',		'S8',	'RW',	'1-byte signed integer')
	csr.dataField('s16',	's16',	'RW',	'2-byte signed integer')
	csr.dataField('S16',	'S16',	'RW',	'2-byte signed integer')
	csr.dataField('s32',	's32',	'RW',	'4-byte signed integer')
	csr.dataField('S32',	'S32',	'RW',	'4-byte signed integer')
	csr.dataField('s64',	's64',	'RW',	'8-byte signed integer')
	csr.dataField('S64',	'S64',	'RW',	'8-byte signed integer')
	csr.dataField('f32',	'f32',	'RW',	'4-byte floating point')
	csr.dataField('F32',	'F32',	'RW',	'4-byte floating point')
	csr.dataField('f64',	'f64',	'RW',	'8-byte floating point')
	csr.dataField('F64',	'F64',	'RW',	'8-byte floating point')
	#
	csr.setByteOffset(190)
	csr.reserveBytes(2)
	csr.reserveBytes(2)
	#
	# Duplicate setByteOffset() calls are ignored.
	#
	csr.setByteOffset(200)
	csr.setByteOffset(200)
	#
	csr.verbatim('/*')
	csr.verbatim('This is multi-line C comment generated from multiple verbatim statements.')
	csr.verbatim('*/')
	csr.verbatim(""":/*
				: * This is multi-line C comment.
				: * Second line of the C comment.
				: * Third line of the C comment.
				: * Final line of the C comment.
				: */""")
	csr.verbatim('// This is single line C++ comment.')
	csr.comment()
	csr.dataField('u8_field',	'u8',	'RW',	'1-byte unsigned integer')
	csr.dataField('U8_field',	'U8',	'RW',	'1-byte unsigned integer')
	csr.dataField('u16_field',	'u16',	'RW',	'2-byte unsigned integer')
	csr.dataField('U16_field',	'U16',	'RW',	'2-byte unsigned integer')
	#
	csr.standardCsrRegisters()
	csr.endCsr()
	csr.endFile()


def test_5(testNumber, argv):
	csr.beginFile('Csr_Compiler_Test_' + str(testNumber), 254, (0,1,2), argv = argv, path = 'test_suite',
			brief	= 'Test #' + str(testNumber) + ' of CSR compiler',
			detail	= """
						This is a test of the CSR compiler and is not intended for actual use.
						
						""" + csr.StandardCsrDescription)
	csr.define(' CSR_TEST ', "({})".format(testNumber),
			brief	= ' Test Number ',
			detail	= """
						This value indicates the CSR test number.
						""")
	csr.beginCsr()
	#
	csr.dataField('u8a',	csr.u8,			'RW',	'u8')
	csr.dataField('u8b',	csr.uint8,		'RW',	'uint8')
	csr.dataField('u8c',	csr.uint8_t,	'RW',	'uint8_t')
	csr.dataField('u8d',	csr.quint8,		'RW',	'quint8')
	#
	csr.dataField('u16a',	csr.u16,		'RW',	'u16')
	csr.dataField('u16b',	csr.uint16,		'RW',	'uint16')
	csr.dataField('u16c',	csr.uint16_t,	'RW',	'uint16_t')
	csr.dataField('u16d',	csr.quint16,	'RW',	'quint16')
	#
	csr.dataField('u32a',	csr.u32,		'RW',	'u32')
	csr.dataField('u32b',	csr.uint32,		'RW',	'uint32')
	csr.dataField('u32c',	csr.uint32_t,	'RW',	'uint32_t')
	csr.dataField('u32d',	csr.quint32,	'RW',	'quint32')
	#
	csr.dataField('u64a',	csr.u64,		'RW',	'u64')
	csr.dataField('u64b',	csr.uint64,		'RW',	'uint64')
	csr.dataField('u64c',	csr.uint64_t,	'RW',	'uint64_t')
	csr.dataField('u64d',	csr.quint64,	'RW',	'quint64')
	#
	csr.dataField('s8a',	csr.s8,			'RW',	's8')
	csr.dataField('s8b',	csr.sint8,		'RW',	'sint8')
	csr.dataField('s8c',	csr.int8_t,		'RW',	'int8_t')
	csr.dataField('s8d',	csr.qint8,		'RW',	'qint8')
	#
	csr.dataField('s16a',	csr.s16,		'RW',	's16')
	csr.dataField('s16b',	csr.sint16,		'RW',	'sint16')
	csr.dataField('s16c',	csr.int16_t,	'RW',	'int16_t')
	csr.dataField('s16d',	csr.qint16,		'RW',	'qint16')
	#
	csr.dataField('s32a',	csr.s32,		'RW',	's32')
	csr.dataField('s32b',	csr.sint32,		'RW',	'sint32')
	csr.dataField('s32c',	csr.int32_t,	'RW',	'int32_t')
	csr.dataField('s32d',	csr.qint32,		'RW',	'qint32')
	#
	csr.dataField('s64a',	csr.s64,		'RW',	's64')
	csr.dataField('s64b',	csr.sint64,		'RW',	'sint64')
	csr.dataField('s64c',	csr.int64_t,	'RW',	'int64_t')
	csr.dataField('s64d',	csr.qint64,		'RW',	'qint64')
	#
	csr.dataField('f32a',	csr.f32,		'RW',	'f32')
	csr.dataField('f32b',	csr.float,		'RW',	'float')
	csr.dataField('f64a',	csr.f64,		'RW',	'f64')
	csr.dataField('f64b',	csr.double,		'RW',	'double')
	csr.dataField('f64c',	csr.qreal,		'RW',	'qreal')
	#
	csr.standardCsrRegisters()
	csr.endCsr()
	csr.endFile()

# Test code.
if __name__ == "__main__":
	defaultOptions = sys.argv[1:] + "--outdir ./tst/".split()
	test_1(1, defaultOptions + "--outfile tst1".split())
	test_2(2, defaultOptions + "--outfile tst2".split())
	test_3(3, defaultOptions + "--outfile tst3".split())
	test_4(4, defaultOptions + "--outfile tst4".split())
	test_5(5, defaultOptions + "--outfile tst5".split())
