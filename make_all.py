import csr
import sys

import attitude_sensor
import camera_adapter
import comm_hub
import led_controller
import motor_controller
import power_converter


# Test code.
if __name__ == "__main__":
	defaultOptions = sys.argv[1:] + "--include inc/ --control ctl/ --listing lst/".split()
	attitude_sensor.attitude_sensor(defaultOptions + "--outfile attitude_sensor".split())
	#
	camera_adapter.camera_adapter(defaultOptions + "--outfile camera_adapter".split())
	#
	comm_hub.comm_hub(defaultOptions + "--outfile comm_hub".split())
	#
	led_controller.led_controller(defaultOptions + "--outfile led_controller".split())
	#
	motor_controller.motor_controller(defaultOptions + "--outfile motor_controller".split())
	#
	power_converter.power_converter(defaultOptions + "--outfile power_converter".split())
